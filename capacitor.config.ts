import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.subcargoclientes.globalhap',
  appName: 'subcargo-clientes',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
