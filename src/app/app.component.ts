import { Component } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { OnesignalService } from './services/onesignal.service';
import { LocalStorageService } from './services/localstorage.service';
import { Router } from '@angular/router';
import { registerLocaleData } from '@angular/common';

import localeEsCL from '@angular/common/locales/es-CL'
registerLocaleData(localeEsCL, 'es-CL')
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private router: Router,
    private platform: Platform,
    private onesignalService: OnesignalService,
    private localStorageService: LocalStorageService,
    private menuController: MenuController
  ) {this.initializeSubcargoWebApp()}

  initializeSubcargoWebApp() {
    this.platform.ready().then(() => {
      if(this.platform.is('ios')){
        this.onesignalService.setPlatformType('ios')
      }else{
        this.onesignalService.setPlatformType('android')
      }
      this.localStorageService.getValueFromLocalStorage('user').then((key) => {
        console.log(key)
        console.log(JSON.stringify(key))
        if(key.value){
          this.localStorageService.user = JSON.parse(key.value)
          this.router.navigate(['/tabs/dashboard'], {replaceUrl: true}); 
        }
      })
      setTimeout(() => {
        if(this.localStorageService.user)
          this.onesignalService.setupOneSignalSystem(this.localStorageService.user._id)
      }, 5000)
    })
  }
  cerrarSesion(){
    this.onesignalService.platformType = undefined
    this.onesignalService.disconnectedUser()
    this.localStorageService.saveDataInLocalStorage('user',undefined)
    this.localStorageService.saveDataInLocalStorage('token', undefined)
    this.menuController.toggle()
    this.router.navigate(['/walkthrough'], {replaceUrl: true})
  }
}
