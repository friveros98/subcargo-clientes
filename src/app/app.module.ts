import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP } from '@ionic-native/http/ngx';
import { ExcludePipe } from './pipes/exclude.pipe';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { DisplayTypeByEnumAndArrayPipe } from './pipes/display-type-by-enum-and-array.pipe';
import { ParseIntPipe } from './pipes/parse-int.pipe';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, GooglePlaceModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, HTTP, OneSignal],
  bootstrap: [AppComponent],
})
export class AppModule {}
