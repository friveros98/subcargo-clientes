import { Component, Input, OnInit } from '@angular/core';
import { Solicitud } from 'src/app/providers/solicitudes/solicitudes.types';
import { constants, LISTA_DE_CARGAS } from 'src/environments/constants/constants';
import { EstadoSolicitud } from 'src/environments/constants/enums';
import { SolicitudOptions } from '../ficha/ficha.types';

@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.scss'],
})
export class SolicitudComponent implements OnInit {
  @Input() options: SolicitudOptions;
  @Input() valueToDisplay: Solicitud;
  estadoSolicitud = EstadoSolicitud
  objectKeys: any;
  listaDeCargas = LISTA_DE_CARGAS
  constructor() { }

  ngOnInit() {
    console.log(this.valueToDisplay)
  }
  displayModalOfertas(){
    console.log('1')
  }
  displayModalDetallesSolicitud(){
    console.log('2')
  }
  displayModalMensajes(){
    console.log('3')
  }
}
