import { ModalController } from "@ionic/angular";
import { AlertService } from "src/app/services/alert.service";
import { LoaderService } from "src/app/services/loader.service";
import { GenericProvider } from "src/environments/constants/generalTypes";

export interface FichaOptions {
    modalPageIfEditButtonIsTrue: any,
    modalControllerInstance: ModalController
    displayDeleteButton: boolean,
    displayEditButton: boolean,
    providerInstance: GenericProvider
    loaderInstance: LoaderService,
    alertInstance: AlertService,
    propertiesToExclude: string[]
}
export interface SolicitudOptions {
    shouldDisplayStatusBar: boolean,
    modalPageOnClick: any,
    modalControllerInstance: ModalController
    shouldDisplayActionButton: boolean,
    providerInstance: GenericProvider
    loaderInstance: LoaderService,
    alertInstance: AlertService,
    propertiesToExclude: string[]
}