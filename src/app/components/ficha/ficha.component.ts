import { Component, Input, OnInit } from '@angular/core';
import { DestinatariosProvider } from 'src/app/providers/destinatarios/destinatarios.provider';
import { FichaOptions } from './ficha.types';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.scss'],
})
export class FichaComponent implements OnInit {
  @Input() options: FichaOptions;
  @Input() valueToDisplay: any;
  objectKeys: any;
  constructor(
  ) { }

  ngOnInit() {
    this.objectKeys = Object.keys;
    console.log(this.valueToDisplay);
    console.log(this.options.providerInstance)
  }
  deleteEntry(valueToDisplay: any) {
    this.options.loaderInstance.presentLoading()
    this.options.providerInstance.deleteEntry(valueToDisplay._id).then(
      (res) => {
        console.log(JSON.stringify(res))
        this.options.loaderInstance.stopLoader()
        if(res.status == 200){
          this.options.alertInstance.showAlertOnlyOk('Se ha borrado exitosamente!', 'checkmark-outline', undefined, 'errorFormAlert')
        }
      },(err) => {
        console.log(JSON.stringify(err))
        this.options.loaderInstance.stopLoader()
        this.options.alertInstance.showAlertOnlyOk('Hubo un error al intentar eliminarlo, pruebe nuevamente', 'close-outline', undefined, 'errorFormAlert')
      })
  }
  async editEntry(value) {
    let modal = await this.options.modalControllerInstance.create({
      component: this.options.modalPageIfEditButtonIsTrue,
      componentProps: {
        obj: value
      }
    })
      
    return await modal.present();
  }
}
