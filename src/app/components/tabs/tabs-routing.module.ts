import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
        }]
      },{
          path: 'mis-ofertas',
          children: [{
            path: '',
            loadChildren: () => import('../../pages/mis-ofertas/mis-ofertas.module').then(m => m.MisOfertasPageModule)
        }]
      },{
        path: 'mis-servicios',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/mis-servicios/mis-servicios.module').then(m => m.MisServiciosPageModule)
        }]
      },{
        path: 'crear-solicitud',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/crear-solicitud/crear-solicitud.module').then(m => m.CrearSolicitudPageModule)
        }]
      },{
        path: 'destinatarios',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/destinatarios/destinatarios.module').then(m => m.DestinatariosPageModule)
        }]
      },{
        path: 'calendario',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/calendario/calendario.module').then(m => m.CalendarioPageModule)
        }]
      },{
        path: 'reporteria',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/reporteria/reporteria.module').then(m => m.ReporteriaPageModule)
        }]
      },{
        path: 'linea',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/linea/linea.module').then(m => m.LineaPageModule)
        }]
      },{
        path: 'contactanos',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/contactanos/contactanos.module').then(m => m.ContactanosPageModule)
        }]
      },{
        path: 'notificaciones',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/notificaciones/notificaciones.module').then(m => m.NotificacionesPageModule)
        }]
      },{
        path: 'mis-datos',
        children: [{
          path: '',
          loadChildren: () => import('../../pages/mis-datos/mis-datos.module').then(m => m.MisDatosPageModule)
        }]
      }
    ]
  }, {
    path: '',
    redirectTo: 'tabs/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
