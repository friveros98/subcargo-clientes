import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'walkthrough',
    loadChildren: () => import('./pages/walkthrough/walkthrough.module').then( m => m.WalkthroughPageModule)
  },
  { path: 'tabs', loadChildren: () => import('./components/tabs/tabs.module').then(m => m.TabsPageModule) },
  {
    path: '',
    redirectTo: 'walkthrough',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./components/tabs/tabs.module').then( m => m.TabsPageModule)
  },  {
    path: 'mis-ofertas',
    loadChildren: () => import('./pages/mis-ofertas/mis-ofertas.module').then( m => m.MisOfertasPageModule)
  },
  {
    path: 'mis-servicios',
    loadChildren: () => import('./pages/mis-servicios/mis-servicios.module').then( m => m.MisServiciosPageModule)
  },
  {
    path: 'crear-solicitud',
    loadChildren: () => import('./pages/crear-solicitud/crear-solicitud.module').then( m => m.CrearSolicitudPageModule)
  },
  {
    path: 'destinatarios',
    loadChildren: () => import('./pages/destinatarios/destinatarios.module').then( m => m.DestinatariosPageModule)
  },
  {
    path: 'calendario',
    loadChildren: () => import('./pages/calendario/calendario.module').then( m => m.CalendarioPageModule)
  },
  {
    path: 'reporteria',
    loadChildren: () => import('./pages/reporteria/reporteria.module').then( m => m.ReporteriaPageModule)
  },
  {
    path: 'linea',
    loadChildren: () => import('./pages/linea/linea.module').then( m => m.LineaPageModule)
  },
  {
    path: 'contactanos',
    loadChildren: () => import('./pages/contactanos/contactanos.module').then( m => m.ContactanosPageModule)
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('./pages/notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
  },
  {
    path: 'mis-datos',
    loadChildren: () => import('./pages/mis-datos/mis-datos.module').then( m => m.MisDatosPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
