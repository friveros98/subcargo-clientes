import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'parseIntPipe'
})
export class ParseIntPipe implements PipeTransform {

  transform(number: string): number {
    let regex = /[0-9]+/
    if(regex.test(number))
      return parseInt(number)

    return 0;
  }

}
