import { Pipe, PipeTransform } from '@angular/core';
import * as lodash from 'lodash';

@Pipe({
  name: 'excludePipe'
})
export class ExcludePipe implements PipeTransform {

  transform(keys: string[], excludingValues: string[]): string[] {
    let finalArray = lodash.filter(keys, (key) => {
      return !excludingValues.includes(key)
    });
    return finalArray
  }

}
