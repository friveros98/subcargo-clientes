import { Injectable } from "@angular/core";
import { QueryParams } from "src/environments/constants/generalTypes";
import { Camion, CamionDTO, CamposFacturacion, CamposFacturacionDTO, CamposOpcionales, CamposOpcionalesDTO, Checks, ChecksDTO, Cliente, ClienteDTO, Comision, ComisionDTO, Conductor, ConductorDTO, DetalleSobrecostos, DetalleSobrecostosDTO, Oferta, OfertaDTO, Recorrido, RecorridoDTO, Solicitud, SolicitudDTO, Transportista, TransportistaDTO } from "./solicitudes.types";

@Injectable({
    providedIn: 'root'
})
export class SolicitudesProviderMapper {
    constructor(){}
    mapQueryParamsToString(query: QueryParams): string {
        let stringifiedQuery = '?';
        let objectKeys = Object.keys(query)
        for(let i = 0; i < objectKeys.length; i++) {
            if(i != 0 && i != objectKeys.length - 1)
            {
                stringifiedQuery += '&'
                stringifiedQuery += objectKeys[i]
                stringifiedQuery += '='
                stringifiedQuery += JSON.stringify(query[objectKeys[i]])
            }
        }
        console.log(stringifiedQuery)
        return stringifiedQuery
    }
    mapArrayOfertasDTOToArrayObj(ofertasDTO: OfertaDTO[]): Oferta[] {
        let ofertas = [] as Oferta[]
        ofertasDTO.forEach((ofertaDTO) => {
            ofertas.push(this.mapOfertaDTOToObj(ofertaDTO))
        })
        return ofertas
    }
    mapArrayRecorridoDTOtoArrayObj(recorridosDTO: RecorridoDTO[]): Recorrido[] {
        let recorridos = [] as Recorrido[]
        recorridosDTO.forEach((recorridoDTO) => {
            recorridos.push(this.mapRecorridoDTOtoObj(recorridoDTO))
        })
        return recorridos
    }
    mapArrayCamposOpcionalesDTOtoArrayObj(camposOpcionalesDTO: CamposOpcionalesDTO[]): CamposOpcionales[]{
        let camposOpcionales = [] as CamposOpcionales[]
        camposOpcionalesDTO.forEach((campoOpcionalDTO) => {
            camposOpcionales.push(this.mapCamposOpcionalesDTOtoObj(campoOpcionalDTO))
        })
        return camposOpcionales
    }
    mapArrayCamposFacturacionDTOtoArrayObj(camposFacturacionDTO: CamposFacturacionDTO[]): CamposFacturacion[] {
        let camposFacturacion = [] as CamposFacturacion[]
        camposFacturacionDTO.forEach((campoFacturacion) => {
            camposFacturacion.push(this.mapCamposFacturacionDTOtoObj(campoFacturacion))
        })
        return camposFacturacion
    }
    mapArrayComisionDTOtoArrayObj(comisionesDTO: ComisionDTO[]): Comision[] {
        let comisiones = [] as Comision[]
        comisionesDTO.forEach((comisionDTO) => {
            comisiones.push(this.mapComisionDTOtoObj(comisionDTO))
        })
        return comisiones
    }
    mapDetalleSobrecostosDTOtoObj(detalleSobrecostosDTO:DetalleSobrecostosDTO): DetalleSobrecostos{
        let sobrecostos = {} as DetalleSobrecostos
        sobrecostos.terminado = detalleSobrecostosDTO.Terminado,
        sobrecostos.valorPorHoraDeEsperaExtra = detalleSobrecostosDTO.ValorPorHoraDeEsperaExtra,
        sobrecostos.sobreEstadiaRadioUrbano2_15 = detalleSobrecostosDTO.SobreEstadiaRadioUrbano2_15,
        sobrecostos.sobreEstadiaRadioUrbano15_28 = detalleSobrecostosDTO.SobreEstadiaRadioUrbano15_28,
        sobrecostos.sobreEstadiaRadioInterurbano2_15 = detalleSobrecostosDTO.SobreEstadiaRadioInterurbano2_15,
        sobrecostos.sobreEstadiaRadioInterurbano15_28 = detalleSobrecostosDTO.SobreEstadiaRadioInterurbano15_28,
        sobrecostos.fleteFalsoOInterurbano2_15 = detalleSobrecostosDTO.FleteFalsoOInterurbano2_15,
        sobrecostos.fleteFalsoOInterurbano15_28 = detalleSobrecostosDTO.FleteFalsoOInterurbano15_28,
        sobrecostos.fleteDeRetorno = detalleSobrecostosDTO.FleteDeRetorno,
        sobrecostos.peoneta = detalleSobrecostosDTO.Peoneta,
        sobrecostos.dobleConductor = detalleSobrecostosDTO.DobleConductor,
        sobrecostos.campoOpcional = detalleSobrecostosDTO.CampoOpcional,
        sobrecostos.valorCampoOpcional = detalleSobrecostosDTO.ValorCampoOpcional,
        sobrecostos.valorCampoOpcional1 = detalleSobrecostosDTO.ValorCampoOpcional1,
        sobrecostos.valorCampoOpcional2 = detalleSobrecostosDTO.ValorCampoOpcional2,
        sobrecostos.valorDescuentoSubcargo = detalleSobrecostosDTO.ValorDescuentoSubcargo,
        sobrecostos.valorCamposSubcargo = detalleSobrecostosDTO.ValorCamposSubcargo,
        sobrecostos.descuento = detalleSobrecostosDTO.Descuento,
        sobrecostos.valorDescuento = detalleSobrecostosDTO.ValorDescuento
        return sobrecostos
    }
    mapOfertaDTOToObj(ofertaDTO: OfertaDTO): Oferta {
        let oferta = {} as Oferta
        oferta._id = ofertaDTO._id,
        oferta.solicitud = ofertaDTO.Solicitud,
        oferta.conductor = ofertaDTO.Conductor,
        oferta.transportista = ofertaDTO.Transportista,
        oferta.camion = ofertaDTO.Camion,
        oferta.oferta = ofertaDTO.Oferta,
        oferta.createdAt = ofertaDTO.createdAt,
        oferta.updatedAt = ofertaDTO.updatedAt,
        oferta.aceptada = ofertaDTO.Aceptada,
        oferta.detalleEncuesta = ofertaDTO.DetalleEncuesta,
        oferta.opcionEncuesta = ofertaDTO.OpcionEncuesta
        return oferta
    }
    mapClienteDTOtoObj(clienteDTO: ClienteDTO): Cliente {
        let cliente = {} as Cliente
        cliente._id = clienteDTO._id,
        cliente.nombre = clienteDTO.Nombre
        return cliente
    }
    mapRecorridoDTOtoObj(recorridoDTO: RecorridoDTO): Recorrido{
        let recorrido = {} as Recorrido
        recorrido.tipo = recorridoDTO.tipo,
        recorrido._id = recorridoDTO._id,
        recorrido.direccion = recorridoDTO.Direccion,
        recorrido.lat = recorridoDTO.lat,
        recorrido.lng = recorridoDTO.lng,
        recorrido.nombre = recorridoDTO.nombre,
        recorrido.telefono = recorridoDTO.telefono,
        recorrido.email = recorridoDTO.email
        return recorrido
    }
    mapComisionDTOtoObj(comisionDTO: ComisionDTO): Comision {
        let comision = {} as Comision
        comision._id = comisionDTO._id,
        comision.interesPorTipoDePago30 = comisionDTO.InteresPorTipoDePago30,
        comision.interesPorTipoDePago60 = comisionDTO.InteresPorTipoDePago60,
        comision.interesPorTipoDePago90 = comisionDTO.InteresPorTipoDePago90,
        comision.comisionSubcargo = comisionDTO.ComisionSubcargo,
        comision.comisionCancelarCliente = comisionDTO.ComisionCancelarCliente,
        comision.comisionCancelarTransportista = comisionDTO.ComisionCancelarTransportista,
        comision.rango0A500MercaderiaGeneral = comisionDTO.Rango0A500MercaderiaGeneral,
        comision.rango501A1000MercaderiaGeneral = comisionDTO.Rango501A1000MercaderiaGeneral,
        comision.rango1001A3000MercaderiaGeneral = comisionDTO.Rango1001A3000MercaderiaGeneral,
        comision.rango0A500MercaderiaPeligrosa = comisionDTO.Rango0A500MercaderiaPeligrosa,
        comision.rango501A1000MercaderiaPeligrosa = comisionDTO.Rango501A1000MercaderiaPeligrosa,
        comision.rango1001A3000MercaderiaPeligrosa = comisionDTO.Rango1001A3000MercaderiaPeligrosa,
        comision.primaMinimaTipoA = comisionDTO.PrimaMinimaTipoA,
        comision.cantidadDeHorasDeEsperaSinCobro = comisionDTO.CantidadDeHorasDeEsperaSinCobro,
        comision.valorPorHoraDeEsperaExtra = comisionDTO.ValorPorHoraDeEsperaExtra,
        comision.sobreEstadiaRadioUrbano2_15 = comisionDTO.SobreEstadiaRadioUrbano2_15,
        comision.sobreEstadiaRadioUrbano15_28 = comisionDTO.SobreEstadiaRadioUrbano15_28,
        comision.sobreEstadiaRadioInterurbano2_15 = comisionDTO.SobreEstadiaRadioInterurbano2_15,
        comision.sobreEstadiaRadioInterurbano15_28 = comisionDTO.SobreEstadiaRadioInterurbano15_28,
        comision.fleteFalsoOInterurbano2_15 = comisionDTO.FleteFalsoOInterurbano2_15,
        comision.topeFleteFalso2_15 = comisionDTO.TopeFleteFalso2_15,
        comision.fleteFalsoOInterurbano15_28 = comisionDTO.FleteFalsoOInterurbano15_28,
        comision.topeFleteFalso15_28 = comisionDTO.TopeFleteFalso15_28,
        comision.fleteDeRetorno = comisionDTO.FleteDeRetorno,
        comision.peoneta = comisionDTO.Peoneta,
        comision.dobleConductor = comisionDTO.DobleConductor,
        comision.tituloCampoOpcional1 = comisionDTO.TituloCampoOpcional1,
        comision.valorCampoOpcional1 = comisionDTO.ValorCampoOpcional1,
        //comision.checks = this.mapChecksDTOtoObj(comisionDTO.Checks),
        comision.cliente = comisionDTO.Cliente,
        comision.createdAt = comisionDTO.createdAt,
        comision.updatedAt = comisionDTO.updatedAt,
        comision.cantidadDeHorasDeEsperaSinCobroInterurbano = comisionDTO.CantidadDeHorasDeEsperaSinCobroInterurbano
        return comision
    }
    mapChecksDTOtoObj(ChecksDTO: ChecksDTO): Checks {
        let checks = {} as Checks
        checks.valorPorHoraDeEsperaExtracheck = ChecksDTO.ValorPorHoraDeEsperaExtracheck ? ChecksDTO.ValorPorHoraDeEsperaExtracheck: false,
        checks.cantidadDeHorasDeEsperaSinCobrocheck = ChecksDTO.CantidadDeHorasDeEsperaSinCobrocheck ? ChecksDTO.CantidadDeHorasDeEsperaSinCobrocheck: false
        checks.sobreEstadiaRadioUrbano2_15check = ChecksDTO.SobreEstadiaRadioUrbano2_15check ? ChecksDTO.SobreEstadiaRadioUrbano2_15check: false
        checks.sobreEstadiaRadioUrbano15_28check = ChecksDTO.SobreEstadiaRadioUrbano15_28check ? ChecksDTO.SobreEstadiaRadioUrbano15_28check: false
        checks.sobreEstadiaRadioInterurbano2_15check = ChecksDTO.SobreEstadiaRadioInterurbano2_15check ? ChecksDTO.SobreEstadiaRadioInterurbano2_15check: false
        checks.sobreEstadiaRadioInterurbano15_28check = ChecksDTO.SobreEstadiaRadioInterurbano15_28check ? ChecksDTO.SobreEstadiaRadioInterurbano15_28check: false
        checks.fleteFalsoOInterurbano2_15check = ChecksDTO.FleteFalsoOInterurbano2_15check ? ChecksDTO.FleteFalsoOInterurbano2_15check: false
        checks.fleteFalsoOInterurbano15_28check = ChecksDTO.FleteFalsoOInterurbano15_28check ? ChecksDTO.FleteFalsoOInterurbano15_28check: false
        checks.fleteDeRetornocheck = ChecksDTO.FleteDeRetornocheck ? ChecksDTO.FleteDeRetornocheck: false
        checks.peonetacheck = ChecksDTO.Peonetacheck ? ChecksDTO.Peonetacheck: false
        checks.dobleConductorcheck = ChecksDTO.DobleConductorcheck ? ChecksDTO.DobleConductorcheck: false
        checks.campoOpcionalcheck = ChecksDTO.CampoOpcionalcheck ? ChecksDTO.CampoOpcionalcheck: false
        checks.campoOpcional2check = ChecksDTO.CampoOpcional2check ? ChecksDTO.CampoOpcional2check: false
        return checks
    }
    mapCamposOpcionalesDTOtoObj(camposOpcionalesDTO: CamposOpcionalesDTO): CamposOpcionales {
        let camposOpcionales = {} as CamposOpcionales
        camposOpcionales.titulo = camposOpcionalesDTO.Titulo,
        camposOpcionales.valor = camposOpcionalesDTO.Valor,
        camposOpcionales._id = camposOpcionalesDTO._id
        return camposOpcionales
    }
    mapCamposFacturacionDTOtoObj(camposFacturacionDTO: CamposFacturacionDTO): CamposFacturacion {
        let camposFacturacion = {} as CamposFacturacion
        camposFacturacion.titulo = camposFacturacionDTO.Titulo,
        camposFacturacion.valor = camposFacturacionDTO.Valor,
        camposFacturacion._id = camposFacturacionDTO._id
        return camposFacturacion
    }
    mapCamionDTOtoObj(camionDTO: CamionDTO): Camion {
        let camion = {} as Camion
        camion._id = camionDTO._id,
        camion.valoracion = camionDTO.Valoracion,
        camion.minPallets = camionDTO.MinPallets,
        camion.maxPallets = camionDTO.MaxPallets,
        camion.estado = camionDTO.Estado,
        camion.estadoDocumentos = camionDTO.EstadoDocumentos,
        camion.gpsActive = camionDTO.GPSActive,
        camion.operativoHasta_UF = camionDTO.OperativoHasta_UF,
        camion.modelo = camionDTO.Modelo,
        camion.ano = camionDTO.Ano,
        camion.patente = camionDTO.Patente,
        camion.tipoDeCamion = camionDTO.TipoDeCamion,
        camion.iInscripcionPorRubro = camionDTO.InscripcionPorRubro,
        camion.dAlto = camionDTO.DAlto,
        camion.dAncho = camionDTO.DAncho,
        camion.dLargo = camionDTO.DLargo,
        camion.tipoDeEstructuraDeCarga = camionDTO.TipoDeEstructuraDeCarga,
        camion.tipoDeEstructuraDeCarga2 = camionDTO.TipoDeEstructuraDeCarga,
        camion.camionPorTipoDeEje = camionDTO.CamionPorTipoDeEje,
        camion.capacidadMaximaCamion = camionDTO.CapacidadMaximaCamion,
        camion.capacidadMinimaCamion = camionDTO.CapacidadMinimaCamion,
        camion.documentos = camionDTO.Documentos,
        camion.fechaDeVencimientoPermisoDeCirculacion = camionDTO.FechaDeVencimientoPermisoDeCirculacion,
        camion.fechaDeVencimientoRevisionTecnica = camionDTO.FechaDeVencimientoRevisionTecnica,
        camion.fechaDeVencimientoSeguroObligatorio = camionDTO.FechaDeVencimientoSeguroObligatorio,
        camion.fechaDeVencimientoSeguroDeCarga = camionDTO.FechaDeVencimientoSeguroDeCarga,
        camion.botonDePanico = camionDTO.BotonDePanico,
        camion.cobertura = camionDTO.Cobertura,
        camion.coberturaSeguroDeCarga = camionDTO.CoberturaSeguroDeCarga,
        camion.codigoGPS = camionDTO.CodigoGPS,
        camion.detectorDeInhibicion = camionDTO.DetectorDeInhibicion,
        camion.gps = camionDTO.GPS,
        camion.nombreCompaniaSeguros = camionDTO.NombreCompaniaSeguros,
        camion.nombreEmpresaAsosiadaGPS = camionDTO.NombreEmpresaAsosiadaGPS,
        camion.nombreEmpresaAsosiadaSeguroDeCarga = camionDTO.NombreEmpresaAsosiadaSeguroDeCarga,
        camion.numeroDePoliza = camionDTO.NumeroDePoliza,
        camion.numeroDePolizaSeguroDeCarga = camionDTO.NumeroDePolizaSeguroDeCarga,
        camion.seguro = camionDTO.Seguro,
        camion.seguroDeCarga = camionDTO.SeguroDeCarga,
        camion.sensoresDeAperturaPilotoCopiloto = camionDTO.SensoresDeAperturaPilotoCopiloto,
        camion.sensoresDeAperturaZonaDeCarga = camionDTO.SensoresDeAperturaZonaDeCarga,
        camion.region = camionDTO.Region,
        camion.comuna = camionDTO.Comuna,
        camion.transportista = camionDTO.Transportista,
        camion.createdAt = camionDTO.createdAt,
        camion.updatedAt = camionDTO.updatedAt,
        camion.conductor = camionDTO.Conductor,
        camion.acople = camionDTO.Acople,
        camion.patenteAcople = camionDTO.PatenteAcople,
        camion.vencimientoPermisoCirculacionAcople = camionDTO.VencimientoPermisoCirculacionAcople,
        camion.vencimientoRevisionTecnicaAcople = camionDTO.VencimientoRevisionTecnicaAcople,
        camion.vencimientoSeguroObligatorioAcople = camionDTO.VencimientoSeguroObligatorioAcople,
        camion.mts3 = camionDTO.Mts3,
        camion.marca = camionDTO.Marca
        return camion
    }
    mapConductorDTOtoObj(conductorDTO: ConductorDTO): Conductor {
        let conductor = {} as Conductor
        conductor._id = conductorDTO._id
        conductor.valoracion = conductorDTO.Valoracion
        conductor.estado = conductorDTO.Estado
        conductor.emailVerified = conductorDTO.emailVerified
        conductor.comisiones = conductorDTO.Comisiones ? this.mapArrayComisionDTOtoArrayObj(conductorDTO.Comisiones): null
        conductor.camposOpcionalesSolicitud = conductorDTO.CamposOpcionalesSolicitud
        conductor.camposFacturacionSolicitud = conductorDTO.CamposFacturacionSolicitud
        conductor.nombreConductor = conductorDTO.NombreConductor
        conductor.apellidoConductor = conductorDTO.ApellidoConductor
        conductor.username = conductorDTO.username
        conductor.email = conductorDTO.email
        conductor.puedeOfertar = conductorDTO.PuedeOfertar
        conductor.telefonoConductor = conductorDTO.TelefonoConductor
        conductor.documentosConductor = conductorDTO.DocumentosConductor
        conductor.fechaDeVencimientoCarnetDeIdentidad = conductorDTO.FechaDeVencimientoCarnetDeIdentidad
        conductor.fechaDeVencimientoLicenciaDeConducir = conductorDTO.FechaDeVencimientoLicenciaDeConducir
        conductor.esConductor = conductorDTO.EsConductor
        conductor.transportista = conductorDTO.Transportista
        conductor.estadoDocumentos = conductorDTO.EstadoDocumentos
        conductor.createdAt = conductorDTO.createdAt
        conductor.updatedAt = conductorDTO.updatedAt
        return conductor
    }
    mapTransportistaDTOtoObj(transportistaDTO: TransportistaDTO): Transportista {
        let transportista = {} as Transportista
        transportista._id = transportistaDTO._id,
        transportista.valoracion = transportistaDTO.Valoracion,
        transportista.estado = transportistaDTO.Estado,
        transportista.emailVerified = transportistaDTO.emailVerified,
        transportista.comisiones = this.mapArrayComisionDTOtoArrayObj(transportistaDTO.Comisiones),
        transportista.camposOpcionalesSolicitud = transportistaDTO.CamposOpcionalesSolicitud,
        transportista.camposFacturacionSolicitud = transportistaDTO.CamposFacturacionSolicitud,
        transportista.username = transportistaDTO.username,
        transportista.nombre = transportistaDTO.Nombre,
        transportista.email = transportistaDTO.email,
        transportista.telefono = transportistaDTO.Telefono,
        transportista.tipoDePago = transportistaDTO.TipoDePago,
        transportista.esTransportista = transportistaDTO.EsTransportista,
        transportista.tieneLinea = transportistaDTO.TieneLinea,
        transportista.linea = transportistaDTO.Linea,
        transportista.direccionComercial = transportistaDTO.DireccionComercial,
        transportista.emailComercial = transportistaDTO.EmailComercial,
        transportista.region = transportistaDTO.Region,
        transportista.comuna = transportistaDTO.Comuna,
        transportista.createdAt = transportistaDTO.createdAt,
        transportista.updatedAt = transportistaDTO.updatedAt,
        transportista.banco = transportistaDTO.Banco,
        transportista.correoElectronicoComprobante = transportistaDTO.CorreoElectronicoComprobante,
        transportista.nombreTitularCuenta = transportistaDTO.NombreTitularCuenta,
        transportista.numeroDeCuenta = transportistaDTO.NumeroDeCuenta,
        transportista.rutTitularCuenta = transportistaDTO.RutTitularCuenta,
        transportista.tipoDeCuenta = transportistaDTO.TipoDeCuenta
        return transportista
    }
    mapDTOtoObj(solicitudDTO: SolicitudDTO): Solicitud {
        let solicitud = {} as Solicitud
        solicitud._id = solicitudDTO._id
        solicitud.detalleSobrecostos = this.mapDetalleSobrecostosDTOtoObj(solicitudDTO.DetalleSobrecostos)
        solicitud.ofertas = solicitudDTO.Ofertas
        solicitud.estado = solicitudDTO.Estado
        solicitud.estadoFacturacion = solicitudDTO.EstadoFacturacion
        solicitud.montoSobrecostos = solicitudDTO.MontoSobrecostos
        solicitud.nPallets = solicitudDTO.NPallets
        solicitud.mts3 = solicitudDTO.Mts3
        solicitud.terminadoCliente = solicitudDTO.TerminadoCliente
        solicitud.montoTotalCliente = solicitudDTO.MontoTotalCliente
        solicitud.montoTotalTransportista = solicitudDTO.MontoTotalTransportista
        solicitud.montoCancelarCliente = solicitudDTO.MontoCancelarCliente
        solicitud.montoCancelarTransportista = solicitudDTO.MontoCancelarTransportista
        solicitud.montoSobrecostosCliente = solicitudDTO.MontoSobrecostosCliente
        solicitud.nombreClienteDeDestino = solicitudDTO.NombreClienteDeDestino
        solicitud.latDeInicio = solicitudDTO.latDeInicio
        solicitud.lonDeInicio = solicitudDTO.LonDeInicio
        solicitud.latDeTermino = solicitudDTO.latDeTermino
        solicitud.lonDeTermino = solicitudDTO.LonDeTermino
        solicitud.direccionDeInicio = solicitudDTO.DireccionDeInicio
        solicitud.direccionDeTermino = solicitudDTO.DireccionDeTermino
        solicitud.cargaATransportar = solicitudDTO.CargaATransportar
        solicitud.cargaATransportarLista = solicitudDTO.CargaATransportarLista
        solicitud.valorDeCargaATransportar = solicitudDTO.ValorDeCargaATransportar
        solicitud.tipoDeEstructuraDeCarga = solicitudDTO.TipoDeEstructuraDeCarga
        solicitud.tipoDeCarga = solicitudDTO.TipoDeCarga
        solicitud.cantidadDeCarga = solicitudDTO.CantidadDeCarga
        solicitud.tipoDeCamion = solicitudDTO.TipoDeCamion
        solicitud.dAlto = solicitudDTO.DAlto
        solicitud.dAncho = solicitudDTO.DAncho
        solicitud.dLargo = solicitudDTO.DLargo
        solicitud.necesitaPersonalExtra = solicitudDTO.NecesitaPersonalExtra
        solicitud.tipoDeSolicitud = solicitudDTO.TipoDeSolicitud
        solicitud.montoNetoInicial = solicitudDTO.MontoNetoInicial
        solicitud.disposicionNetaAPagar = solicitudDTO.DisposicionNetaAPagar
        solicitud.duracionDeRuta = solicitudDTO.DuracionDeRuta
        solicitud.cantidadDeKM = solicitudDTO.CantidadDeKM
        solicitud.cliente = this.mapClienteDTOtoObj(solicitudDTO.Cliente)
        solicitud.recorrido = this.mapArrayRecorridoDTOtoArrayObj(solicitudDTO.Recorrido)
        solicitud.pasoRecorrido = solicitudDTO.PasoRecorrido
        solicitud.camposOpcionales = this.mapArrayCamposOpcionalesDTOtoArrayObj(solicitudDTO.CamposOpcionales)
        solicitud.camposFacturacion = this.mapArrayCamposFacturacionDTOtoArrayObj(solicitudDTO.CamposFacturacion)
        solicitud.clientePrePago = solicitudDTO.ClientePrePago
        solicitud.comision = solicitudDTO.Comision
        solicitud.createdAt = solicitudDTO.createdAt
        solicitud.updatedAt = solicitudDTO.updatedAt
        solicitud.creadoPor = solicitudDTO.CreadoPor
        solicitud.fechaDeSolicitudDeTransporte = solicitudDTO.FechaDeSolicitudDeTransporte
        solicitud.fechaEstimadaDeTermino = solicitudDTO.FechaEstimadaDeTermino
        solicitud.fechaEstimadaDeTerminoString = solicitudDTO.FechaEstimadaDeTerminoString
        solicitud.fechaInicioString = solicitudDTO.FechaInicioString
        solicitud.horaDeLlegada = solicitudDTO.HoraDeLlegada
        solicitud.idSolicitudDeTransporte = solicitudDTO.IdSolicitudDeTransporte
        solicitud.montoSeguroDeCarga = solicitudDTO.MontoSeguroDeCarga
        solicitud.numero = solicitudDTO.Numero
        solicitud.pactando = solicitudDTO.Pactando
        solicitud.sugerencia = solicitudDTO.Sugerencia
        solicitud.aceptadoPor = solicitudDTO.AceptadoPor
        solicitud.acople = solicitudDTO.Acople
        solicitud.camion = solicitudDTO.Camion ? this.mapCamionDTOtoObj(solicitudDTO.Camion):null
        solicitud.conductor = solicitudDTO.Conductor ? this.mapConductorDTOtoObj(solicitudDTO.Conductor):null
        solicitud.fechaAgendado = solicitudDTO.FechaAgendado
        solicitud.lineaRestada = solicitudDTO.LineaRestada
        solicitud.montoNetoPactado = solicitudDTO.MontoNetoPactado
        solicitud.transportista = solicitudDTO.Transportista ? this.mapTransportistaDTOtoObj(solicitudDTO.Transportista):null
        solicitud.fechaTermino = solicitudDTO.FechaTermino
        solicitud.freemium = solicitudDTO.Freemium
        solicitud.valoracion = solicitudDTO.Valoracion
        solicitud.valoracionCamion = solicitudDTO.ValoracionCamion
        solicitud.valoracionConductor = solicitudDTO.ValoracionConductor
        solicitud.fechaCierreSobrecosto = solicitudDTO.FechaCierreSobrecosto
        solicitud.usuarioCierreSobrecosto = solicitudDTO.UsuarioCierreSobrecosto
        solicitud.autorTerminadoCliente = solicitudDTO.AutorTerminadoCliente
        solicitud.fechaTerminadoCliente = solicitudDTO.FechaTerminadoCliente
        return solicitud
    }                            
}