import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/services/http-base.service';
import { LocalStorageService } from 'src/app/services/localstorage.service';
import { constants } from 'src/environments/constants/constants';
import { HttpBasicResponse, QueryParams } from 'src/environments/constants/generalTypes';
import { SolicitudesProviderMapper } from './solicitudes.provider.mapper';

@Injectable({
  providedIn: 'root'
})
export class SolicitudesProvider {
  private CLIENTES_ENDPOINT = '/clientes'
  private SOLICITUDES_ENDPOINT = '/solicitudes_paginadas'
  constructor(
    private solicitudesProviderMapper: SolicitudesProviderMapper,
    private localStorageService: LocalStorageService,
    private httpBaseService: HttpBaseService
  ) { }

  getEntries(query: QueryParams): Promise<HttpBasicResponse> {
    let stringifiedQuery = this.solicitudesProviderMapper.mapQueryParamsToString(query);
    return this.httpBaseService.get(constants.API_URL + this.CLIENTES_ENDPOINT + '/' + this.localStorageService.user._id + this.SOLICITUDES_ENDPOINT + stringifiedQuery)
  }
  deleteEntry(id: string): Promise<HttpBasicResponse> {
    return this.httpBaseService.delete(constants.API_URL + this.CLIENTES_ENDPOINT + '/' + this.localStorageService.user._id + this.SOLICITUDES_ENDPOINT)
  }
}
