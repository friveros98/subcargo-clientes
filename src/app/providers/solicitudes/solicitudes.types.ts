import { Banco, EstadoFacturacion, EstadoSolicitud, InscripcionPorRubro, OpcionEncuesta, SiNoString, TipoDeCamion, TipoDeCarga, TipoDeCuenta, TipoDeEstructuraDeCarga, TipoDePago, TipoDeSolicitud } from 'src/environments/constants/enums';

export interface DetalleSobrecostosDTO {
    Terminado: boolean,
    ValorPorHoraDeEsperaExtra: number,
    SobreEstadiaRadioUrbano2_15: number,
    SobreEstadiaRadioUrbano15_28: number,
    SobreEstadiaRadioInterurbano2_15: number,
    SobreEstadiaRadioInterurbano15_28: number,
    FleteFalsoOInterurbano2_15: number,
    FleteFalsoOInterurbano15_28: number,
    FleteDeRetorno: number,
    Peoneta: number,
    DobleConductor: number,
    CampoOpcional: string,
    ValorCampoOpcional: number,
    ValorCampoOpcional1: number,
    ValorCampoOpcional2: number,
    ValorDescuentoSubcargo: number,
    ValorCamposSubcargo: number,
    Descuento: string,
    ValorDescuento: number
}
export interface DetalleSobrecostos {
    terminado: boolean,
    valorPorHoraDeEsperaExtra: number,
    sobreEstadiaRadioUrbano2_15: number,
    sobreEstadiaRadioUrbano15_28: number,
    sobreEstadiaRadioInterurbano2_15: number,
    sobreEstadiaRadioInterurbano15_28: number,
    fleteFalsoOInterurbano2_15: number,
    fleteFalsoOInterurbano15_28: number,
    fleteDeRetorno: number,
    peoneta: number,
    dobleConductor: number,
    campoOpcional: string,
    valorCampoOpcional: number,
    valorCampoOpcional1: number,
    valorCampoOpcional2: number,
    valorDescuentoSubcargo: number,
    valorCamposSubcargo: number,
    descuento: string,
    valorDescuento: number
}
export interface OfertaDTO {
    _id: string,
    Solicitud: string,
    Conductor: string,
    Transportista: string,
    Camion: string,
    Oferta: number,
    createdAt: string,
    updatedAt: string,
    Aceptada:  boolean,
    DetalleEncuesta: string,
    OpcionEncuesta: OpcionEncuesta
}
export interface Oferta {
    _id: string,
    solicitud: string,
    conductor: string,
    transportista: string,
    camion: string,
    oferta: number,
    createdAt: string,
    updatedAt: string,
    aceptada:  boolean,
    detalleEncuesta: string,
    opcionEncuesta: OpcionEncuesta
}
export interface ClienteDTO {
    _id: string,
	Nombre: string
}
export interface Cliente {
    _id: string,
	nombre: string
}
export interface RecorridoDTO {
    tipo: string,
	_id: string,
	Direccion: string,
	lat: number,
	lng: number,
	nombre: string,
	telefono: string,
	email: string
}
export interface Recorrido {
    tipo: string,
	_id: string,
	direccion: string,
	lat: number,
	lng: number,
	nombre: string,
	telefono: string,
	email?: string
}
export interface CamposOpcionalesDTO {
    Titulo:  string,
	Valor: string,
	_id: string
}
export interface CamposOpcionales {
    titulo:  string,
	valor: string,
	_id: string
}
export interface CamposFacturacionDTO {
    Titulo: string,
	Valor: string,
	_id: string
}
export interface CamposFacturacion {
    titulo: string,
	valor: string,
	_id: string
}
export interface ComisionDTO {
    _id: string,
    InteresPorTipoDePago30: number,
    InteresPorTipoDePago60: number,
    InteresPorTipoDePago90: number,
    ComisionSubcargo: number,
    ComisionCancelarCliente: number,
    ComisionCancelarTransportista: number,
    Rango0A500MercaderiaGeneral: number,
    Rango501A1000MercaderiaGeneral: number,
    Rango1001A3000MercaderiaGeneral: number,
    Rango0A500MercaderiaPeligrosa: number,
    Rango501A1000MercaderiaPeligrosa: number,
    Rango1001A3000MercaderiaPeligrosa: number,
    PrimaMinimaTipoA: number,
    CantidadDeHorasDeEsperaSinCobro: number,
    ValorPorHoraDeEsperaExtra: number,
    SobreEstadiaRadioUrbano2_15: number,
    SobreEstadiaRadioUrbano15_28: number,
    SobreEstadiaRadioInterurbano2_15: number,
    SobreEstadiaRadioInterurbano15_28: number,
    FleteFalsoOInterurbano2_15: number,
    TopeFleteFalso2_15: number,
    FleteFalsoOInterurbano15_28: number,
    TopeFleteFalso15_28: number,
    FleteDeRetorno: number,
    Peoneta: number,
    DobleConductor: number,
    TituloCampoOpcional1: string,
    ValorCampoOpcional1: number,
    Checks?: ChecksDTO,
    Cliente: string,
    createdAt: string,
    updatedAt: string,
    CantidadDeHorasDeEsperaSinCobroInterurbano: number
}
export interface Comision {
    _id: string,
    interesPorTipoDePago30: number,
    interesPorTipoDePago60: number,
    interesPorTipoDePago90: number,
    comisionSubcargo: number,
    comisionCancelarCliente: number,
    comisionCancelarTransportista: number,
    rango0A500MercaderiaGeneral: number,
    rango501A1000MercaderiaGeneral: number,
    rango1001A3000MercaderiaGeneral: number,
    rango0A500MercaderiaPeligrosa: number,
    rango501A1000MercaderiaPeligrosa: number,
    rango1001A3000MercaderiaPeligrosa: number,
    primaMinimaTipoA: number,
    cantidadDeHorasDeEsperaSinCobro: number,
    valorPorHoraDeEsperaExtra: number,
    sobreEstadiaRadioUrbano2_15: number,
    sobreEstadiaRadioUrbano15_28: number,
    sobreEstadiaRadioInterurbano2_15: number,
    sobreEstadiaRadioInterurbano15_28: number,
    fleteFalsoOInterurbano2_15: number,
    topeFleteFalso2_15: number,
    fleteFalsoOInterurbano15_28: number,
    topeFleteFalso15_28: number,
    fleteDeRetorno: number,
    peoneta: number,
    dobleConductor: number,
    tituloCampoOpcional1: string,
    valorCampoOpcional1: number,
    checks?: Checks,
    cliente: string,
    createdAt: string,
    updatedAt: string,
    cantidadDeHorasDeEsperaSinCobroInterurbano: number
}
export interface ChecksDTO {
    ValorPorHoraDeEsperaExtracheck?: boolean,
    CantidadDeHorasDeEsperaSinCobrocheck?: boolean,
    SobreEstadiaRadioUrbano2_15check?: boolean,
    SobreEstadiaRadioUrbano15_28check?: boolean,
    SobreEstadiaRadioInterurbano2_15check?: boolean,
    SobreEstadiaRadioInterurbano15_28check?: boolean,
    FleteFalsoOInterurbano2_15check?: boolean,
    FleteFalsoOInterurbano15_28check?: boolean,
    FleteDeRetornocheck?: boolean,
    Peonetacheck?: boolean,
    DobleConductorcheck?: boolean,
    CampoOpcionalcheck?: boolean,
    CampoOpcional2check?: boolean
}
export interface Checks {
    valorPorHoraDeEsperaExtracheck?: boolean,
    cantidadDeHorasDeEsperaSinCobrocheck?: boolean,
    sobreEstadiaRadioUrbano2_15check?: boolean,
    sobreEstadiaRadioUrbano15_28check?: boolean,
    sobreEstadiaRadioInterurbano2_15check?: boolean,
    sobreEstadiaRadioInterurbano15_28check?: boolean,
    fleteFalsoOInterurbano2_15check?: boolean,
    fleteFalsoOInterurbano15_28check?: boolean,
    fleteDeRetornocheck?: boolean,
    peonetacheck?: boolean,
    dobleConductorcheck?: boolean,
    campoOpcionalcheck?: boolean,
    campoOpcional2check?: boolean
}
export interface Valoracion {
    valor: number,
    numero: number
}
export interface CamionDTO {
    _id: string,
    Valoracion: Valoracion,
    MinPallets: number,
    MaxPallets: number,
    Estado: string,
    EstadoDocumentos: number,
    GPSActive: boolean,
    OperativoHasta_UF: number,
    Modelo: string,
    Ano: number,
    Patente: string,
    TipoDeCamion: TipoDeCamion,
    InscripcionPorRubro: InscripcionPorRubro,
    DAlto: number,
    DAncho: number,
    DLargo: number,
    TipoDeEstructuraDeCarga: TipoDeEstructuraDeCarga,
    TipoDeEstructuraDeCarga2: TipoDeEstructuraDeCarga,
    CamionPorTipoDeEje: number,
    CapacidadMaximaCamion: number,
    CapacidadMinimaCamion: number,
    Documentos: boolean,
    FechaDeVencimientoPermisoDeCirculacion: string,
    FechaDeVencimientoRevisionTecnica: string,
    FechaDeVencimientoSeguroObligatorio: string,
    FechaDeVencimientoSeguroDeCarga: string,
    BotonDePanico: SiNoString,
    Cobertura: string,
    CoberturaSeguroDeCarga: string,
    CodigoGPS: string,
    DetectorDeInhibicion: SiNoString,
    GPS: boolean,
    NombreCompaniaSeguros: string,
    NombreEmpresaAsosiadaGPS: string,
    NombreEmpresaAsosiadaSeguroDeCarga: string,
    NumeroDePoliza: string,
    NumeroDePolizaSeguroDeCarga: string,
    Seguro: boolean,
    SeguroDeCarga: boolean,
    SensoresDeAperturaPilotoCopiloto: SiNoString,
    SensoresDeAperturaZonaDeCarga: SiNoString,
    Region: string,
    Comuna: string,
    Transportista: string,
    createdAt: string,
    updatedAt: string,
    Conductor: string,
    Acople: string,
    PatenteAcople: string,
    VencimientoPermisoCirculacionAcople: string,
    VencimientoRevisionTecnicaAcople: string,
    VencimientoSeguroObligatorioAcople: string,
    Mts3: boolean,
    Marca: string
}
export interface Camion {
    _id: string,
    valoracion: Valoracion,
    minPallets: number,
    maxPallets: number,
    estado: string,
    estadoDocumentos: number,
    gpsActive: boolean,
    operativoHasta_UF: number,
    modelo: string,
    ano: number,
    patente: string,
    tipoDeCamion: TipoDeCamion,
    iInscripcionPorRubro: InscripcionPorRubro,
    dAlto: number,
    dAncho: number,
    dLargo: number,
    tipoDeEstructuraDeCarga: TipoDeEstructuraDeCarga,
    tipoDeEstructuraDeCarga2: TipoDeEstructuraDeCarga,
    camionPorTipoDeEje: number,
    capacidadMaximaCamion: number,
    capacidadMinimaCamion: number,
    documentos: boolean,
    fechaDeVencimientoPermisoDeCirculacion: string,
    fechaDeVencimientoRevisionTecnica: string,
    fechaDeVencimientoSeguroObligatorio: string,
    fechaDeVencimientoSeguroDeCarga: string,
    botonDePanico: SiNoString,
    cobertura: string,
    coberturaSeguroDeCarga: string,
    codigoGPS: string,
    detectorDeInhibicion: SiNoString,
    gps: boolean,
    nombreCompaniaSeguros: string,
    nombreEmpresaAsosiadaGPS: string,
    nombreEmpresaAsosiadaSeguroDeCarga: string,
    numeroDePoliza: string,
    numeroDePolizaSeguroDeCarga: string,
    seguro: boolean,
    seguroDeCarga: boolean,
    sensoresDeAperturaPilotoCopiloto: SiNoString,
    sensoresDeAperturaZonaDeCarga: SiNoString,
    region: string,
    comuna: string,
    transportista: string,
    createdAt: string,
    updatedAt: string,
    conductor: string,
    acople: string,
    patenteAcople: string,
    vencimientoPermisoCirculacionAcople: string,
    vencimientoRevisionTecnicaAcople: string,
    vencimientoSeguroObligatorioAcople: string,
    mts3: boolean,
    marca: string
}
export interface ConductorDTO {
    _id: string,
    Valoracion: Valoracion,
    Estado: string,
    emailVerified: boolean,
    Comisiones: ComisionDTO[],
    CamposOpcionalesSolicitud: string[],
    CamposFacturacionSolicitud: string[],
    NombreConductor: string,
    ApellidoConductor: string,
    username: string,
    email: string,
    PuedeOfertar: boolean,
    TelefonoConductor: string,
    DocumentosConductor: boolean,
    FechaDeVencimientoCarnetDeIdentidad: string,
    FechaDeVencimientoLicenciaDeConducir: string,
    EsConductor: boolean,
    Transportista: string,
    EstadoDocumentos: number,
    createdAt: string,
    updatedAt: string,
}
export interface Conductor {
    _id: string,
    valoracion: Valoracion,
    estado: string,
    emailVerified: boolean,
    comisiones: Comision[],
    camposOpcionalesSolicitud: string[],
    camposFacturacionSolicitud: string[],
    nombreConductor: string,
    apellidoConductor: string,
    username: string,
    email: string,
    puedeOfertar: boolean,
    telefonoConductor: string,
    documentosConductor: boolean,
    fechaDeVencimientoCarnetDeIdentidad: string,
    fechaDeVencimientoLicenciaDeConducir: string,
    esConductor: boolean,
    transportista: string,
    estadoDocumentos: number,
    createdAt: string,
    updatedAt: string,
}
export interface TransportistaDTO {
    _id: string,
    Valoracion: Valoracion,
    Estado: string,
    emailVerified: boolean,
    Comisiones: ComisionDTO[],
    CamposOpcionalesSolicitud: string[],
    CamposFacturacionSolicitud: string[],
    username: string,
    Nombre: string,
    email: string,
    Telefono: string,
    TipoDePago: TipoDePago,
    EsTransportista: true,
    TieneLinea: false,
    Linea: number,
    DireccionComercial: string,
    EmailComercial: string,
    Region: string,
    Comuna: string,
    createdAt: string,
    updatedAt: string,
    Banco: Banco,
    CorreoElectronicoComprobante: string,
    NombreTitularCuenta: string,
    NumeroDeCuenta:string,
    RutTitularCuenta: string,
    TipoDeCuenta: TipoDeCuenta
}
export interface Transportista {
    _id: string,
    valoracion: Valoracion,
    estado: string,
    emailVerified: boolean,
    comisiones: Comision[],
    camposOpcionalesSolicitud: string[],
    camposFacturacionSolicitud: string[],
    username: string,
    nombre: string,
    email: string,
    telefono: string,
    tipoDePago: TipoDePago,
    esTransportista: true,
    tieneLinea: false,
    linea: number,
    direccionComercial: string,
    emailComercial: string,
    region: string,
    comuna: string,
    createdAt: string,
    updatedAt: string,
    banco: Banco,
    correoElectronicoComprobante: string,
    nombreTitularCuenta: string,
    numeroDeCuenta:string,
    rutTitularCuenta: string,
    tipoDeCuenta: TipoDeCuenta
}
export interface SolicitudDTO {
    _id: string,
    DetalleSobrecostos: DetalleSobrecostosDTO,
    Ofertas: string[],
    Estado: EstadoSolicitud,
    EstadoFacturacion: EstadoFacturacion,
    MontoSobrecostos: number,
    NPallets: number,
    Mts3: false,
    TerminadoCliente: true,
    MontoTotalCliente: number,
    MontoTotalTransportista: number,
    MontoCancelarCliente: number,
    MontoCancelarTransportista: number,
    MontoSobrecostosCliente: number,
    NombreClienteDeDestino: string,
    latDeInicio: number,
    LonDeInicio: number,
    latDeTermino: number,
    LonDeTermino: number,
    DireccionDeInicio: string,
    DireccionDeTermino: string,
    CargaATransportar: string,
    CargaATransportarLista: string,
    ValorDeCargaATransportar: number,
    TipoDeEstructuraDeCarga: TipoDeEstructuraDeCarga,
    TipoDeCarga: TipoDeCarga,
    CantidadDeCarga: string,
    TipoDeCamion: TipoDeCamion,
    DAlto: number,
    DAncho: number,
    DLargo: number,
    NecesitaPersonalExtra: string,
    TipoDeSolicitud: TipoDeSolicitud,
    MontoNetoInicial: number,
    DisposicionNetaAPagar: number,
    DuracionDeRuta: string,
    CantidadDeKM: string,
    Cliente: ClienteDTO,
    Recorrido: RecorridoDTO[],
    PasoRecorrido: number,
    CamposOpcionales: CamposOpcionalesDTO[],
    CamposFacturacion: CamposFacturacionDTO[],
    ClientePrePago: number,
    Comision: string,
    createdAt: string,
    updatedAt: string,
    CreadoPor: string,
    FechaDeSolicitudDeTransporte: string,
    FechaEstimadaDeTermino: string,
    FechaEstimadaDeTerminoString?: string,
    FechaInicioString: string,
    HoraDeLlegada: string,
    IdSolicitudDeTransporte: string,
    MontoSeguroDeCarga: number,
    Numero: string,
    Pactando: false,
    Sugerencia: true,
    AceptadoPor: string,
    Acople: string,
    Camion: CamionDTO,
    Conductor: ConductorDTO,
    FechaAgendado: string,
    LineaRestada: number,
    MontoNetoPactado: number,
    Transportista: TransportistaDTO,
    FechaTermino: string,
    Freemium: false,
    Valoracion: number,
    ValoracionCamion: number,
    ValoracionConductor: number,
    FechaCierreSobrecosto: string,
    UsuarioCierreSobrecosto: string,
    AutorTerminadoCliente: string,
    FechaTerminadoCliente: string,
}
export interface Solicitud {
    _id: string,
    detalleSobrecostos: DetalleSobrecostos,
    ofertas: string[],
    estado: EstadoSolicitud,
    estadoFacturacion: EstadoFacturacion,
    montoSobrecostos: number,
    nPallets: number,
    mts3: boolean,
    terminadoCliente: boolean,
    montoTotalCliente: number,
    montoTotalTransportista: number,
    montoCancelarCliente: number,
    montoCancelarTransportista: number,
    montoSobrecostosCliente: number,
    nombreClienteDeDestino: string,
    latDeInicio: number,
    lonDeInicio: number,
    latDeTermino: number,
    lonDeTermino: number,
    direccionDeInicio: string,
    direccionDeTermino: string,
    cargaATransportar: string,
    cargaATransportarLista: string,
    valorDeCargaATransportar: number,
    tipoDeEstructuraDeCarga: TipoDeEstructuraDeCarga,
    tipoDeCarga: TipoDeCarga,
    cantidadDeCarga: string,
    tipoDeCamion: TipoDeCamion,
    dAlto: number,
    dAncho: number,
    dLargo: number,
    necesitaPersonalExtra: string,
    tipoDeSolicitud: TipoDeSolicitud,
    montoNetoInicial: number,
    disposicionNetaAPagar: number,
    duracionDeRuta: string,
    cantidadDeKM: string,
    cliente: Cliente,
    recorrido: Recorrido[],
    pasoRecorrido: number,
    camposOpcionales: CamposOpcionales[],
    camposFacturacion: CamposFacturacion[],
    clientePrePago: number,
    comision: string,
    createdAt: string,
    updatedAt: string,
    creadoPor: string,
    fechaDeSolicitudDeTransporte: string,
    fechaEstimadaDeTermino: string,
    fechaEstimadaDeTerminoString?: string,
    fechaInicioString?: string,
    horaDeLlegada?: string,
    idSolicitudDeTransporte: string,
    montoSeguroDeCarga: number,
    numero: string,
    pactando: false,
    sugerencia: true,
    aceptadoPor: string,
    acople: string,
    camion: Camion,
    conductor: Conductor,
    fechaAgendado: string,
    lineaRestada: number,
    montoNetoPactado: number,
    transportista: Transportista,
    fechaTermino: string,
    freemium: false,
    valoracion: number,
    valoracionCamion: number,
    valoracionConductor: number,
    fechaCierreSobrecosto: string,
    usuarioCierreSobrecosto: string,
    autorTerminadoCliente: string,
    fechaTerminadoCliente: string,
    remitenteTransportista?: boolean
}