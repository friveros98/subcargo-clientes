import { TestBed } from '@angular/core/testing';

import { SolicitudesProvider } from './solicitudes.provider';

describe('SolicitudesProvider', () => {
  let service: SolicitudesProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SolicitudesProvider);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
