import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/services/http-base.service';
import { RegistroUsuario } from './user.types';
import { constants } from '../../../environments/constants/constants';
import { UserProviderMapper } from './user.provider.mapper';
import { HttpBasicResponse } from 'src/environments/constants/generalTypes';
import { LocalStorageService } from 'src/app/services/localstorage.service';
@Injectable({
  providedIn: 'root'
})
export class UserProvider {
  private USERS_ENDPOINT = '/users'
  private USERS_LOGIN_ENDPOINT = '/login'
  private PASSWORD_RECOVER_ENDPOINT = '/solicitar_cambio_clave'
  private PASSWORD_CHANGE_ENDPOINT = '/cambiar_clave'
  private NOTIFICATIONS_ENDPOINT = '/notificaciones'
  constructor(
    private userProviderMapper: UserProviderMapper,
    private HttpBaseService: HttpBaseService,
    private localStorageService: LocalStorageService
  ) { }

  registrarUsuario(usuario: RegistroUsuario): Promise<HttpBasicResponse> {
    const body = this.userProviderMapper.mapRegistroUsuarioToRegistroUsuarioDTO(usuario)
    return this.HttpBaseService.post(constants.API_URL + this.USERS_ENDPOINT, body)
  }
  iniciarSesion(rut: string, password: string): Promise<HttpBasicResponse> {
    const body = this.userProviderMapper.mapLoginDataToLogInBody(rut, password);
    return this.HttpBaseService.post(constants.API_URL + this.USERS_ENDPOINT + this.USERS_LOGIN_ENDPOINT, body)
  }
  recuperarPassword(rut: string): Promise<HttpBasicResponse> {
    return this.HttpBaseService.get(constants.API_URL + this.USERS_ENDPOINT + '/' + rut + this.PASSWORD_RECOVER_ENDPOINT)
  }
  cambiarPassword(codigo: string, rut: string, password: string): Promise<HttpBasicResponse> {
    const body = {
      code: codigo,
      rut: rut,
      password: password
    }
    return this.HttpBaseService.post(constants.API_URL + this.USERS_ENDPOINT + this.PASSWORD_CHANGE_ENDPOINT, body)
  }
  getUserNotifications(): Promise<HttpBasicResponse> {
    return this.HttpBaseService.get(constants.API_URL + this.USERS_ENDPOINT + '/' + this.localStorageService.user._id + this.NOTIFICATIONS_ENDPOINT);
  }
}
