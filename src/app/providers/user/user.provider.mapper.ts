import { Injectable } from "@angular/core";
import { LoginUsuario, RegistroUsuario, RegistroUsuarioDTO, UserNotification, UserNotificationDTO } from "./user.types";
@Injectable({
    providedIn: 'root'
})
export class UserProviderMapper {
    constructor(){}
    
    mapRegistroUsuarioToRegistroUsuarioDTO(usuario: RegistroUsuario): RegistroUsuarioDTO {
        const usuarioDTO = {} as RegistroUsuarioDTO;
        usuarioDTO.Nombre = usuario.nombre;
        usuarioDTO.username = usuario.username;
        usuarioDTO.password = usuario.password;
        usuarioDTO.email = usuario.email;
        usuarioDTO.TipoDePago = usuario.tipoDePago;
        usuarioDTO.Telefono = usuario.telefono;
        usuarioDTO.EsCliente = usuario.esCliente;
        
        if(usuario.direccionComercial)
            usuarioDTO.DireccionComercial = usuario.direccionComercial

        if(usuario.giroDeLaEmpresa)
            usuarioDTO.GiroDeLaEmpresa = usuario.giroDeLaEmpresa;

        if(usuario.representanteLegal)
            usuarioDTO.RepresentanteLegal = usuario.representanteLegal;
        if(usuario.rutRepresentanteLegal)
            usuarioDTO.RutRepresentanteLegal = usuario.rutRepresentanteLegal;
        if(usuario.rubro)
            usuarioDTO.Rubro = usuario.rubro;
        return usuarioDTO;
    }
    mapLoginDataToLogInBody(rut: string, password: string): LoginUsuario {
        const usuario = {} as LoginUsuario
        usuario.username = rut;
        usuario.password = password
        return usuario
    }
    mapNotificationDTOtoObj(notification: UserNotificationDTO): UserNotification {
        let userNotification = {} as UserNotification
        userNotification._id = notification._id;
        userNotification.titulo = notification.Titulo;
        userNotification.usuario = notification.Usuario;
        userNotification.mensaje = notification.Mensaje;
        userNotification.createdAt = notification.createdAt;
        return userNotification
    }
}