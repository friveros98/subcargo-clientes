import { Injectable } from "@angular/core";
import { NgForm } from "@angular/forms";
import { DestinoFrecuente, DestinoFrecuenteDTO } from "./destinatarios.types";

@Injectable({
    providedIn: 'root'
})
export class DestinatariosProviderMapper {
    constructor(){}
    mapFormToObj(form: NgForm, latLng: google.maps.LatLngLiteral): DestinoFrecuente{
        let destinoFrecuente = {} as DestinoFrecuente
        destinoFrecuente.titulo = form.value.titulo,
        destinoFrecuente.nombre =  form.value.nombre,
        destinoFrecuente.email =  form.value.email,
        destinoFrecuente.telefono =  form.value.telefono,
        destinoFrecuente.direccion = form.value.direccion,
        destinoFrecuente.cliente = '603f087a6dc80d0b81748735',
        destinoFrecuente.lat = latLng.lat,
        destinoFrecuente.lng = latLng.lng
        return destinoFrecuente;
    }
    mapObjToDTO(destinoFrecuente: DestinoFrecuente): DestinoFrecuenteDTO{
        let destinoFrecuenteDTO = {} as DestinoFrecuenteDTO
        destinoFrecuenteDTO._id = destinoFrecuente._id
        destinoFrecuenteDTO.Titulo = destinoFrecuente.titulo
        destinoFrecuenteDTO.nombre =  destinoFrecuente.nombre
        destinoFrecuenteDTO.email =  destinoFrecuente.email
        destinoFrecuenteDTO.telefono =  destinoFrecuente.telefono
        destinoFrecuenteDTO.Direccion = destinoFrecuente.direccion
        destinoFrecuenteDTO.Cliente = destinoFrecuente.cliente
        destinoFrecuenteDTO.lat = destinoFrecuente.lat
        destinoFrecuenteDTO.lng = destinoFrecuente.lng
        return destinoFrecuenteDTO;
    }
    mapDTOtoObj(destinoFrecuenteDTO: DestinoFrecuenteDTO): DestinoFrecuente {
        let destinoFrecuente = {} as DestinoFrecuente
        destinoFrecuente._id = destinoFrecuenteDTO._id
        destinoFrecuente.cliente = destinoFrecuenteDTO.Cliente
        destinoFrecuente.direccion = destinoFrecuenteDTO.Direccion
        destinoFrecuente.email = destinoFrecuenteDTO.email
        destinoFrecuente.nombre = destinoFrecuenteDTO.nombre
        destinoFrecuente.telefono = destinoFrecuenteDTO.telefono
        destinoFrecuente.titulo = destinoFrecuenteDTO.Titulo
        destinoFrecuente.lat = destinoFrecuenteDTO.lat
        destinoFrecuente.lng = destinoFrecuenteDTO.lng
        return destinoFrecuente
    }
}