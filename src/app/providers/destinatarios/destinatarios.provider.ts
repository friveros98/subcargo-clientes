import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/services/http-base.service';
import { constants } from 'src/environments/constants/constants';
import { HttpBasicResponse } from 'src/environments/constants/generalTypes';
import { DestinatariosProviderMapper } from './destinatarios.provider.mapper';
import { DestinoFrecuente, DestinoFrecuenteDTO } from './destinatarios.types';

@Injectable({
  providedIn: 'root'
})
export class DestinatariosProvider {
  private USERS_ENDPOINT = '/users';
  private CLIENTS_ENDPOINT = '/clientes';
  private DESTINOS_FRENCUENTES_ENDPOINT = '/direcciones_frecuentes';
  constructor(private HttpBaseService: HttpBaseService, private destinatariosProviderMapper: DestinatariosProviderMapper) { }

  getEntries(idCliente: string = '603f087a6dc80d0b81748735'): Promise<HttpBasicResponse> {
    return this.HttpBaseService.get(constants.API_URL + this.CLIENTS_ENDPOINT + '/' + idCliente + this.DESTINOS_FRENCUENTES_ENDPOINT)
  }
  deleteEntry(idDestinoFrecuente: string = '603f087a6dc80d0b81748735'): Promise<HttpBasicResponse> {
    return this.HttpBaseService.delete(constants.API_URL + this.DESTINOS_FRENCUENTES_ENDPOINT + '/' + idDestinoFrecuente)
  }
  createEntry(idCliente: string = '603f087a6dc80d0b81748735', destinoFrecuente: DestinoFrecuente): Promise<HttpBasicResponse> {
    let destinoFrecuenteDTO = this.destinatariosProviderMapper.mapObjToDTO(destinoFrecuente)
    return this.HttpBaseService.post(constants.API_URL + this.CLIENTS_ENDPOINT + '/' + idCliente + this.DESTINOS_FRENCUENTES_ENDPOINT, destinoFrecuenteDTO)
  }
  updateEntry(destinoFrecuente: DestinoFrecuente): Promise<HttpBasicResponse> {
    let destinoFrecuenteDTO = this.destinatariosProviderMapper.mapObjToDTO(destinoFrecuente)
    return this.HttpBaseService.put(constants.API_URL + this.DESTINOS_FRENCUENTES_ENDPOINT + '/' + destinoFrecuenteDTO._id, destinoFrecuenteDTO)
  }
}
