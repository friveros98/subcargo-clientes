import { TestBed } from '@angular/core/testing';

import { DestinatariosProvider } from './destinatarios.provider';

describe('DestinatariosProvider', () => {
  let service: DestinatariosProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DestinatariosProvider);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
