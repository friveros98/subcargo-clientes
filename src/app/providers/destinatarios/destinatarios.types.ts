export interface DestinoFrecuenteDTO {
    _id: string,
    Titulo: string,
    nombre: string,
    email: string,
    telefono: string,
    Direccion: string,
    Cliente: string,
    lat: number,
    lng: number
}
export interface DestinoFrecuente {
    _id: string,
    titulo: string,
    nombre: string,
    email: string,
    telefono: string,
    direccion: string,
    cliente: string,
    lat: number,
    lng: number
}