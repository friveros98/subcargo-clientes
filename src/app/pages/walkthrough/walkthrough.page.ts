import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular/';


@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.page.html',
  styleUrls: ['./walkthrough.page.scss'],
})

export class WalkthroughPage implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  @ViewChild('videoWT', { read: ElementRef }) videoWT: ElementRef;
  video: HTMLVideoElement
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  
  constructor(private router: Router) { }
  ngOnInit(){

  }
  ionViewWillEnter(){
    this.video = this.videoWT.nativeElement;
    this.video.muted = true;
    this.video.volume = 0;
    this.slides.slideTo(0)

  }
  ngAferViewInit() {
  }  
  
  slideChanged(){
    this.slides.isBeginning().then((isBeginning) =>{
      if(isBeginning == true)
        this.slides.lockSwipeToPrev(true)
      else
        this.slides.lockSwipeToPrev(false)
    })
    this.slides.isEnd().then((istrue) => {
      if(istrue == true)
        this.router.navigate(['/login'], {replaceUrl: true})
    });

  }
}
