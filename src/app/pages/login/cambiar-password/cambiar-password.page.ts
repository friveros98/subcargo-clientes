import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController, AlertController, LoadingController, IonInput } from '@ionic/angular';
// import { RestService } from 'src/app/services/rest/rest.service';

@Component({
  selector: 'app-cambiar-password',
  templateUrl: './cambiar-password.page.html',
  styleUrls: ['./cambiar-password.page.scss'],
})
export class CambiarPasswordPage implements OnInit {
  @ViewChild('inputRut') recoverRut: IonInput
  formRecuperarContra: FormGroup;
  errorMessage: any[];
  guionInput: boolean;

  constructor(
    private modalCambiarPassword: ModalController,
    private alertController: AlertController, 
    private loading: LoadingController,
    private formBuilder: FormBuilder,
    // private restService: RestService
    ) { }

  ngOnInit() {
    this.formRecuperarContra = this.formBuilder.group({
      code: ['', Validators.required],
      rut: ['', Validators.compose([
        Validators.pattern("^\\d{7}(?:\\d{1})?([-])([\\dkK])$"),
        Validators.required
      ])],
      password: ['', Validators.required],
      password2: ['', Validators.required]
    })

    this.errorMessage = [];
    this.errorMessage['rut'] = 'El rut es requerido para recuperar sus datos.'
    this.errorMessage['rutInvalido'] = 'Rut invalido, use el siguiente formato: (12345678-9).'
    this.guionInput = false
  }
  // recuperarPassword(){
  //   this.restService.recuperarPassword(this.formRecuperarContra.value).then((res)=>{
  //     if(res.status == 200){
  //       this.modalCambiarPassword.dismiss()
  //       this.popupAceptadoForm("La contraseña se ha cambiado exitosamente.")
  //     }
  //   })
  // }
  inputRutChange(){
    if(this.recoverRut.value != undefined || this.recoverRut.value != null){
      let aux: string = this.recoverRut.value.toString()
      aux = aux.replace("-","")
      if(aux.length >=1){
        let aux2 = aux.match(/([k0-9])/gi)
        let aux3 = aux2.join("")
        aux = aux3
      }
      if(aux.length >=2){
        let cuerpoRut: any;
        let dv: any;
        cuerpoRut = aux.slice(0,aux.length-1)
        dv = aux.slice(aux.length-1,aux.length)
        aux = cuerpoRut + "-" + dv
      }
      this.recoverRut.value = aux;
    }
  }
  
  cancelarModalRecover(){
    this.modalCambiarPassword.dismiss();
  }
  async popupErrorForm(text:string){
    const alert = await this.alertController.create({
      cssClass: 'errorFormAlert',
      header: '',
      message: '<ion-icon name="close-outline" class="errorIcon"></ion-icon><br><h1 class="alertBody">'+text+'</h1>',
      buttons: [{
        text: 'Ok'}]
    });
    await alert.present();
  }
  async presentLoading(tiempo = 250) {
    const loading = await this.loading.create({
      translucent: true,
      duration: tiempo
    });
    await loading.present();

    return await loading.onDidDismiss();
  }
  async popupAceptadoForm(text:string){
    const alert = await this.alertController.create({
      cssClass: 'errorFormAlert',
      header: '',
      message: '<ion-icon name="checkmark-outline" class="errorIcon"></ion-icon><br><h1 class="alertBody">'+text+'</h1>',
      buttons: [{
        text: 'Ok'}]
    });
    await alert.present();
  }
}
