import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController, LoadingController, AlertController, IonInput, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder, NgForm } from '@angular/forms';
import { LoggedUsuario, LoginUsuario } from 'src/app/providers/user/user.types';
import { RecuperarPasswordPage } from './recuperar-password/recuperar-password.page';
import { RegistroPage } from './registro/registro.page';
import { AlertService } from 'src/app/services/alert.service';
import { UserProvider } from 'src/app/providers/user/user.provider';
import { LoaderService } from 'src/app/services/loader.service';
import { LocalStorageService } from 'src/app/services/localstorage.service';
import { OnesignalService } from 'src/app/services/onesignal.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('usuarioUsername') usuarioUsernameInput: IonInput
  @ViewChild('usuarioPassword') usuarioPasswordInput: IonInput
  usuario = {} as LoginUsuario;
  constructor(
    private alertService: AlertService,
    private router: Router,
    private modal: ModalController,
    private userProvider: UserProvider,
    private loaderService: LoaderService,
    private localStorageService: LocalStorageService,
    private oneSignalService: OnesignalService,
    private tokenService: TokenService
  ) {
  }
  ngOnInit() {
    // this.formLogin = this.formBuilder.group({
    //   rut: ['', Validators.compose([
    //     Validators.pattern("^\\d{7}(?:\\d{1})?([-])([\\dkK])\\s?$"),
    //     Validators.required
    //   ])],
    //   password: ['', Validators.required]
    // })
    // this.errorMessage = [];
    // this.errorMessage['rut'] = 'El rut es requerido para iniciar sesion'
    // this.errorMessage['password'] = 'Contraseña incorrecta'
    // this.errorMessage['rutInvalido'] = 'Rut invalido, use el siguiente formato: (12345678-9).'
  }
  inputRutChange(){
    if(this.usuarioUsernameInput.value != undefined || this.usuarioUsernameInput.value != null){
      let aux: string = this.usuarioUsernameInput.value.toString()
      aux = aux.replace("-","")
      if(aux.length >=1){
        let aux2 = aux.match(/([k0-9])/gi)
        let aux3 = aux2.join("")
        aux = aux3
      }
      if(aux.length >=2){
        let cuerpoRut: any;
        let dv: any;
        cuerpoRut = aux.slice(0,aux.length-1)
        dv = aux.slice(aux.length-1,aux.length)
        aux = cuerpoRut + "-" + dv
      }
      this.usuarioUsernameInput.value = aux;
      this.usuarioUsernameInput.setFocus()
    }
  }
  iniciarSesion(username: string, password: string){
    // this.router.navigate(['/tabs/dashboard'], {replaceUrl: true});
    //   return
    let rutPattern = /\d{7}(?:\d{1})?([-])([\dkK])\s?/
    if(!username){
      this.alertService.showAlertOnlyOk('El rut es requerido para iniciar sesion', 'close-outline', undefined, 'errorFormAlert')
      return;
    }
    if(!rutPattern.test(username)){
      this.alertService.showAlertOnlyOk('Rut invalido, use el siguiente formato: (12345678-9).', 'close-outline', undefined, 'errorFormAlert')
      return
    }
    if(!password) {
      this.alertService.showAlertOnlyOk('La contraseña es requerida para iniciar sesion', 'close-outline', undefined, 'errorFormAlert')
      return;
    }
    console.log(username)
    console.log(password)
    this.loaderService.presentLoading()
    this.userProvider.iniciarSesion(username,password).then((res) => {
      this.loaderService.stopLoader()
      console.log('res: '+JSON.stringify(res))
      let user = JSON.parse(res.body).user as LoggedUsuario
      console.log(user)
      if(res.status == 200) {
        this.tokenService.setCurrentToken(JSON.parse(res.body).token)
        this.localStorageService.saveDataInLocalStorage('user', user)
        this.localStorageService.user = user;
        this.oneSignalService.setupOneSignalSystem(user._id)
        this.router.navigate(['/tabs/dashboard'], {replaceUrl: true}); 
      }
    }, (err) => {
      console.log('err: '+JSON.stringify(err))
      this.alertService.showAlertOnlyOk('Hubo un error al iniciar sesión, pruebe nuevamente o verifique sus credenciales.', 'close-outline', undefined, 'errorFormAlert')
      this.loaderService.stopLoader()
    })   
  }

  async recoverPassword(){
    let modal = await this.modal.create({
      component: RecuperarPasswordPage
    });
    return await modal.present();
  }

  async registerUser(value: boolean) {
    let modal = await this.modal.create({
      component: RegistroPage,
      componentProps: {
        empresaOCliente: value
      }
    })
    
    return await modal.present();
  }
}
