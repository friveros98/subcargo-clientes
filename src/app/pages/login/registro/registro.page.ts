import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController, AlertController, LoadingController, IonInput } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { RestService } from 'src/app/services/rest/rest.service';
import { RegistroTerminosCondicionesPage } from '../registro-terminos-condiciones/registro-terminos-condiciones.page';
import { LoaderService } from 'src/app/services/loader.service';
import { AlertService } from 'src/app/services/alert.service';
import { UserProvider } from 'src/app/providers/user/user.provider';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  @ViewChild('rut') rutInput: IonInput;
  @Input() empresaOCliente: boolean;
  registroForm: FormGroup;
  errorMessage: any[];
  guionInput: boolean;
  selectTipoDePago = {
    cssClass: 'customAlert',
    header: 'Elige el tipo de pago'
  };
  constructor(
    private modal: ModalController,
    private modal2: ModalController,
    private formBuilder: FormBuilder,
    private router: Router,
    public userProvider: UserProvider,
    public alertService: AlertService,
    public loaderService: LoaderService,
  ) {
  }

  ngOnInit() {
    console.log(this.empresaOCliente)
    let form = {
      nombre: ['', Validators.required],
      username: ['', Validators.compose([
        Validators.pattern("^\\d{7}(?:\\d{1})?([-])([\\dkK])$"),
        Validators.required
      ])],
      telefono: ['', Validators.required],
      email: ['', Validators.compose([
        Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
        Validators.required
      ])],
      direccionComercial: ['', Validators.required],
      tipoDePago: ['', Validators.required],
      rutRepresentanteLegal: ['', Validators.compose([
        Validators.pattern("^\\d{7}(?:\\d{1})?([-])([\\dkK])$"),
        Validators.required
      ])],
      rubro: ['', Validators.required],
      giroDeLaEmpresa: ['', Validators.required],
      representanteLegal: ['', Validators.required],
      password: ['', Validators.compose([
        Validators.pattern(/^.{5,}$/),
        Validators.required
      ])],
      password2: ['', Validators.compose([
        Validators.pattern(/^.{5,}$/),
        Validators.required
      ])],
    }

    if(!this.empresaOCliente){
      delete form.direccionComercial
      delete form.tipoDePago
      delete form.rutRepresentanteLegal
      delete form.rubro
      delete form.representanteLegal
      delete form.giroDeLaEmpresa
    }
    this.registroForm = this.formBuilder.group(form);
    this.guionInput = false;
    this.errorMessage = [];
    this.errorMessage['nombre'] = 'Debes ingresar la razón social / nombre.'
    this.errorMessage['rut'] = 'Debes ingresar el Rut.'
    this.errorMessage['rutInvalido'] = 'Rut invalido, use el siguiente formato: (12345678-9).'
    this.errorMessage['telefono'] = 'Debes ingresar un teléfono de contacto.'
    this.errorMessage['email'] = 'Debes ingresar un correo electrónico.'
    this.errorMessage['emailInvalido'] = 'Correo electrónico invalido'
    this.errorMessage['emailcomercialInvalido'] = 'Correo electrónico invalido'
    this.errorMessage['emailcomercial'] = 'Debes ingresar un correo electrónico comercial.'
    this.errorMessage['direccioncomercial'] = 'Debes ingresar una dirección comercial.'
    this.errorMessage['password'] = 'Debes ingresar la contraseña'
    this.errorMessage['passwordInvalido'] = 'La contraseña debe tener un minimo de 6 caracteres'
    this.errorMessage['password2'] = 'Debes ingresar por segunda vez la contraseña'
    this.errorMessage['password2Invalido'] = 'La contraseña debe tener un minimo de 6 caracteres'
    this.errorMessage['passwordDiferentes'] = 'Las constraseñas son diferentes.'
  }
  async registrarse(){
    if(this.registroForm.status == "INVALID"){
      let controls = this.registroForm.controls;
      for (let c in controls) {
        if (controls[c].invalid) {
          if(controls[c].errors.pattern){
            this.alertService.showAlertOnlyOk(this.errorMessage[c+'Invalido'], 'close-outline',undefined, 'errorFormAlert')
            return;
          }
          this.alertService.showAlertOnlyOk(this.errorMessage[c], 'close-outline',undefined, 'errorFormAlert')
          return;
        }
      }
    }
    if(this.registroForm.get('password').value != this.registroForm.get('password2').value){
      this.alertService.showAlertOnlyOk(this.errorMessage['passwordDiferentes'], 'close-outline',undefined, 'errorFormAlert')
      return;
    }
    this.registroTYC();
  }
  async registroTYC() {
    const modal = await this.modal2.create({
      component: RegistroTerminosCondicionesPage,
      componentProps: {
        datos: this.registroForm
      }
    });
    modal.present();
    modal.onDidDismiss()
      .then((data) => {
        if(data['data']==1)
          this.modal.dismiss()
    });    
  }
  rutChange(rutInput: IonInput){
    if(rutInput.value != undefined || rutInput.value != null){
      let aux: string = rutInput.value.toString()
      aux = aux.replace("-","")
      if(aux.length >=1){
        let aux2 = aux.match(/([k0-9])/gi)
        let aux3 = aux2.join("")
        aux = aux3
      }
      if(aux.length >=2){
        let cuerpoRut: any;
        let dv: any;
        cuerpoRut = aux.slice(0,aux.length-1)
        dv = aux.slice(aux.length-1,aux.length)
        aux = cuerpoRut + "-" + dv
      }
      rutInput.value = aux;
    }
  }
  closeModal(){this.modal.dismiss();}
}
