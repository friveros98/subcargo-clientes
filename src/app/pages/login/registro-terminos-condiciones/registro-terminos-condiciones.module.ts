import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistroTerminosCondicionesPageRoutingModule } from './registro-terminos-condiciones-routing.module';

import { RegistroTerminosCondicionesPage } from './registro-terminos-condiciones.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroTerminosCondicionesPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RegistroTerminosCondicionesPage]
})
export class RegistroTerminosCondicionesPageModule {}
