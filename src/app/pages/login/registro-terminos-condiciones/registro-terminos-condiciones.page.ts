import { Component, OnInit, Input } from '@angular/core';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoaderService } from 'src/app/services/loader.service';
import { UserProvider } from 'src/app/providers/user/user.provider';
import { AlertService } from 'src/app/services/alert.service';
import { RegistroUsuario } from 'src/app/providers/user/user.types';
// import { RestService } from 'src/app/services/rest/rest.service';

@Component({
  selector: 'app-registro-terminos-condiciones',
  templateUrl: './registro-terminos-condiciones.page.html',
  styleUrls: ['./registro-terminos-condiciones.page.scss'],
})
export class RegistroTerminosCondicionesPage implements OnInit {
  @Input() datos: any;
  constructor(
    private modal: ModalController,
    private loaderService: LoaderService,
    private userProvider: UserProvider,
    private alertService: AlertService
  ) { }

  ngOnInit() {
  }
  registroCompleto(){
    let usuario = this.datos.value as RegistroUsuario
    usuario.esCliente = true;
    console.log(usuario)
    console.log(JSON.stringify(this.datos.value))
    this.loaderService.presentLoading()
    this.userProvider.registrarUsuario(this.datos.value).then(  
      (res) => {
        console.log("RES.DATA:  ",JSON.stringify(res));  
        this.loaderService.stopLoader()
        if (res.status == 201) {
          this.alertService.showAlertOnlyOk('Te has registrado exitosamente!', 'checkmark-outline',undefined,'errorFormAlert')
          this.modal.dismiss(1);
        }else{
          this.alertService.showAlertOnlyOk('Ha ocurrido un error, por favor intenta nuevamente.', 'close-outline',undefined,'errorFormAlert')
        }
      }, (error) => {
        console.log(error)
        this.loaderService.stopLoader()
        this.alertService.showAlertOnlyOk(
          'Es posible que el rut o correo del usuario ya se encuentre registrado, por favor revise estos datos.',
          'close-outline',
          undefined,
          'errorFormAlert')
      }
    )
  }
  closeModal(){
    this.modal.dismiss();
  }
}
