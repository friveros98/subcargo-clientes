import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroTerminosCondicionesPage } from './registro-terminos-condiciones.page';

const routes: Routes = [
  {
    path: '',
    component: RegistroTerminosCondicionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistroTerminosCondicionesPageRoutingModule {}
