import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController, AlertController, LoadingController, IonInput } from '@ionic/angular';
import { UserProvider } from 'src/app/providers/user/user.provider';
import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';
import { CambiarPasswordPage } from '../cambiar-password/cambiar-password.page';
// import { RestService } from 'src/app/services/rest/rest.service';

@Component({
  selector: 'app-recuperar-password',
  templateUrl: './recuperar-password.page.html',
  styleUrls: ['./recuperar-password.page.scss'],
})
export class RecuperarPasswordPage implements OnInit {
  @ViewChild('inputRut') recoverRut: IonInput
  formRecuperarContra: FormGroup;
  errorMessage: any[];
  guionInput: boolean;
  constructor(
    private modal: ModalController,
    private modalPass: ModalController,
    private alertService: AlertService,
    private userProvider: UserProvider,
    private loader: LoaderService
    ) { }

  ngOnInit() {
  }
  async presentModal(rut:any) {
    const modal = await this.modalPass.create({
      component: CambiarPasswordPage,
      componentProps: {
        rut: rut
      }
    });
    modal.present()
  }
  async recuperarPassword() {
    let rutPattern = /\d{7}(?:\d{1})?([-])([\dkK])\s?/
    if(!rutPattern.test(this.recoverRut.value.toString())) {
      this.alertService.showAlertOkCancel('Rut invalido, use el siguiente formato: (12345678-9).', 'close-outline', undefined, 'formAlert')
      return
    }
    this.recuperarPass()
    this.modal.dismiss();
  }
  recuperarPass(){
    this.loader.presentLoading()
    this.userProvider.recuperarPassword(this.recoverRut.value.toString()).then(  
     (res) => {
       this.loader.stopLoader()
       console.log(res)
       if (res.status == 200) {  
        console.log("MAIL ENVIADO CON EXITO: ", res);
        this.alertService.showAlertOnlyOk("Correo electronico enviado con éxito.",'checkmark-outline', undefined, 'formAlert')
        this.modal.dismiss()
       }
     }, (error) => {
      this.loader.stopLoader()
       console.log("error1: ", error)
     }
   )
  }
  inputRutChange(){
    if(this.recoverRut.value != undefined || this.recoverRut.value != null){
      let aux: string = this.recoverRut.value.toString()
      aux = aux.replace("-","")
      if(aux.length >=1){
        let aux2 = aux.match(/([k0-9])/gi)
        let aux3 = aux2.join("")
        aux = aux3
      }
      if(aux.length >=2){
        let cuerpoRut: any;
        let dv: any;
        cuerpoRut = aux.slice(0,aux.length-1)
        dv = aux.slice(aux.length-1,aux.length)
        aux = cuerpoRut + "-" + dv
      }
      this.recoverRut.value = aux;
    }
  }
  cancelarModalRecover(){
    this.modal.dismiss();
  }
  closeModal(){this.modalPass.dismiss();}
}
