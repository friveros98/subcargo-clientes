import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecuperarPasswordPageRoutingModule } from './recuperar-password-routing.module';

import { RecuperarPasswordPage } from './recuperar-password.page';
import { UserProvider } from 'src/app/providers/user/user.provider';
import { UserProviderMapper } from 'src/app/providers/user/user.provider.mapper';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecuperarPasswordPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RecuperarPasswordPage],
  providers: [UserProvider, UserProviderMapper]
})
export class RecuperarPasswordPageModule {}
