import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FichaOptions } from 'src/app/components/ficha/ficha.types';
import { DestinatariosProvider } from 'src/app/providers/destinatarios/destinatarios.provider';
import { DestinatariosProviderMapper } from 'src/app/providers/destinatarios/destinatarios.provider.mapper';
import { DestinoFrecuente, DestinoFrecuenteDTO } from 'src/app/providers/destinatarios/destinatarios.types';
import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';
import { DestinatariosAddEditPage } from './destinatarios-add-edit/destinatarios-add-edit.page';

@Component({
  selector: 'app-destinatarios',
  templateUrl: './destinatarios.page.html',
  styleUrls: ['./destinatarios.page.scss'],
})
export class DestinatariosPage implements OnInit {
  destinosFrecuente: DestinoFrecuente[]
  fichaOptions: FichaOptions;
  constructor(
    private destinatariosProvider: DestinatariosProvider,
    private destinatariosProviderMapper: DestinatariosProviderMapper,
    private loaderService: LoaderService,
    private alertService: AlertService,
    private modal: ModalController
  ) { }

  ngOnInit() {
    this.fichaOptions = {
      modalPageIfEditButtonIsTrue: DestinatariosAddEditPage,
      modalControllerInstance: this.modal,
      displayDeleteButton: true,
      displayEditButton: true,
      providerInstance: this.destinatariosProvider,
      loaderInstance: this.loaderService,
      alertInstance: this.alertService,
      propertiesToExclude: ['_id', 'lat', 'lng', 'cliente']
    }
    this.loadDestinosFrecuentes()
  }
  loadDestinosFrecuentes($event?) {
    if(!$event) 
      this.loaderService.presentLoading()
    this.destinatariosProvider.getEntries().then((res) => {
      if(!$event)
        this.loaderService.stopLoader()
      else
        $event.target.complete()
      console.log('res: '+JSON.stringify(res))
      if(res.status == 200) {
        let destinosDTO = (JSON.parse(res.body) as DestinoFrecuenteDTO[])
        this.destinosFrecuente = []
        destinosDTO.forEach((destinoDTO) => {
          this.destinosFrecuente.push(this.destinatariosProviderMapper.mapDTOtoObj(destinoDTO))
        })
      }
    }, (err) => {
      console.log('err: '+JSON.stringify(err))
      this.alertService.showAlertOnlyOk('Hubo un error al obtener los destinos frecuentes, pruebe nuevamente o reinicie su sesion', 'close-outline', undefined, 'errorFormAlert')
      if($event)
        $event.target.complete()
    })   
  }
  async addNewEntry() {
    let modal = await this.modal.create({
      component: DestinatariosAddEditPage,
    })
      
    return await modal.present();
  }
}
