import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinatariosPageRoutingModule } from './destinatarios-routing.module';

import { DestinatariosPage } from './destinatarios.page';
import { FichaComponent } from 'src/app/components/ficha/ficha.component';
import { ExcludePipe } from 'src/app/pipes/exclude.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinatariosPageRoutingModule
  ],
  declarations: [DestinatariosPage, FichaComponent, ExcludePipe]
})
export class DestinatariosPageModule {}
