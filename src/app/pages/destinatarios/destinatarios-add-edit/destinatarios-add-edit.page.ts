import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GoogleMap, MapMarker, MapMarkerClusterer } from '@angular/google-maps';
import { ModalController } from '@ionic/angular';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { DestinatariosProvider } from 'src/app/providers/destinatarios/destinatarios.provider';
import { DestinatariosProviderMapper } from 'src/app/providers/destinatarios/destinatarios.provider.mapper';
import { DestinoFrecuente } from 'src/app/providers/destinatarios/destinatarios.types';
import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-destinatarios-add-edit',
  templateUrl: './destinatarios-add-edit.page.html',
  styleUrls: ['./destinatarios-add-edit.page.scss'],
})
export class DestinatariosAddEditPage implements OnInit {
  @ViewChild('maps') googlemaps: GoogleMap
  @ViewChild('marker') googleMarker: any
  @Input() obj: DestinoFrecuente;
  selectedAddress: Address;
  options = {
    types : [],
    componentRestrictions: { country: 'CL' }
  }
  mapCenter: google.maps.LatLngLiteral = {lat: 40, lng: -20}
  positions: google.maps.LatLngLiteral[] = [];
  constructor(
    private modal: ModalController,
    private alertService: AlertService,
    private loaderService: LoaderService,
    private destinatariosProvider: DestinatariosProvider,
    private destinatariosProviderMapper: DestinatariosProviderMapper
  ) { }

  ngOnInit() {
    if(!this.obj)
      this.obj = {} as DestinoFrecuente;
    else {
      this.selectedAddress = {} as Address
      this.mapCenter.lat = this.obj.lat
      this.mapCenter.lng = this.obj.lng
      this.positions.push(this.mapCenter)
    }
  }
  updateGoogleMaps($event) {
    this.selectedAddress = $event.formatted_address;
    this.mapCenter.lat = $event.geometry.location.lat()
    this.mapCenter.lng = $event.geometry.location.lng()
    this.obj.direccion = $event.formatted_address;
    this.googlemaps.googleMap.setCenter({lat: $event.geometry.location.lat(), lng: $event.geometry.location.lng()})
    this.positions = []
    this.positions.push({lat: $event.geometry.location.lat(), lng: $event.geometry.location.lng()})
  }
  createNewDestinatarioFrecuente(form: NgForm){
    console.log(form)
    if(!form.valid){
      this.alertService.showAlertOnlyOk('Por favor rellene todos los campos correctamente.', 'close-outline', undefined, 'errorFormAlert')
      return
    }

    console.log(form.value)
    if(this.obj._id){
      let updatedEntry = this.destinatariosProviderMapper.mapFormToObj(form, this.positions[0])
      updatedEntry._id = this.obj._id
      this.updateEntry(updatedEntry);
      return;
    }
    this.createEntry(this.destinatariosProviderMapper.mapFormToObj(form, this.positions[0]))
  }
  updateEntry(destinoFrecuente: DestinoFrecuente){
    this.destinatariosProvider.updateEntry(destinoFrecuente).then((res) => {
      this.loaderService.stopLoader()
      console.log('res: '+JSON.stringify(res))
      if(res.status == 200) {
        this.alertService.showAlertOnlyOk('Destino frecuente actualizado con exito!', 'checkmark-outline', undefined, 'errorFormAlert')
        this.closeModal()
      }
    }, (err) => {
      console.log('err: '+JSON.stringify(err))
      this.alertService.showAlertOnlyOk('Hubo un error al actualizar el destino frecuente, intente nuevamente', 'close-outline', undefined, 'errorFormAlert')
      this.loaderService.stopLoader()
    })  
  }
  createEntry(destinoFrecuente: DestinoFrecuente) {
    this.destinatariosProvider.createEntry('', destinoFrecuente).then((res) => {
      this.loaderService.stopLoader()
      console.log('res: '+JSON.stringify(res))
      if(res.status == 200) {
        this.alertService.showAlertOnlyOk('Destino frecuente creado con exito!', 'checkmark-outline', undefined, 'errorFormAlert')
        this.closeModal()
      }
    }, (err) => {
      console.log('err: '+JSON.stringify(err))
      this.alertService.showAlertOnlyOk('Hubo un error al crear el destino frecuente, intente nuevamente', 'close-outline', undefined, 'errorFormAlert')
      this.loaderService.stopLoader()
    })   
  }
  closeModal(){this.modal.dismiss()}
}
