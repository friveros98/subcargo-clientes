import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinatariosAddEditPageRoutingModule } from './destinatarios-add-edit-routing.module';

import { DestinatariosAddEditPage } from './destinatarios-add-edit.page';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinatariosAddEditPageRoutingModule,
    GooglePlaceModule,
    GoogleMapsModule
  ],
  declarations: [DestinatariosAddEditPage]
})
export class DestinatariosAddEditPageModule {}
