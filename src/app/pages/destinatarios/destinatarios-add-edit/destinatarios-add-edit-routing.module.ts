import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DestinatariosAddEditPage } from './destinatarios-add-edit.page';

const routes: Routes = [
  {
    path: '',
    component: DestinatariosAddEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinatariosAddEditPageRoutingModule {}
