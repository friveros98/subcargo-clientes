import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { UserProvider } from 'src/app/providers/user/user.provider';
import { UserProviderMapper } from 'src/app/providers/user/user.provider.mapper';
import { UserNotification, UserNotificationDTO } from 'src/app/providers/user/user.types';
import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';
// import { DataStorageService } from 'src/app/services/datastorage/datastorage.service';
// import { RestService } from 'src/app/services/rest/rest.service';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {
  notificaciones: any[];
  constructor(
    private loaderService: LoaderService,
    private userProvider: UserProvider,
    private userProviderMapper: UserProviderMapper,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.loadUserNotifications()
  }
  loadUserNotifications() {
    this.loaderService.presentLoading()
    this.userProvider.getUserNotifications().then((res) => {
      this.loaderService.stopLoader()
      console.log('res: '+JSON.stringify(res))
      if(res.status == 200) {
        let notificacionesDTO = JSON.parse(res.body) as UserNotificationDTO[]
        this.notificaciones = [] as UserNotification[]
        notificacionesDTO.forEach((dto) => {
          this.notificaciones.push(this.userProviderMapper.mapNotificationDTOtoObj(dto))
        })
      }
    }, (err) => {
      console.log('err: '+JSON.stringify(err))
      this.alertService.showAlertOnlyOk('Hubo un error al obtener las notificaciones, pruebe nuevamente o re-inicie su sesion', 'close-outline', undefined, 'errorFormAlert')
      this.loaderService.stopLoader()
    }) 
  }
}
