import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SolicitudOptions } from 'src/app/components/ficha/ficha.types';
import { SolicitudesProvider } from 'src/app/providers/solicitudes/solicitudes.provider';
import { SolicitudesProviderMapper } from 'src/app/providers/solicitudes/solicitudes.provider.mapper';
import { Camion, Conductor, Solicitud, SolicitudDTO } from 'src/app/providers/solicitudes/solicitudes.types';
import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';
import { EstadoFacturacion, EstadoSolicitud, TipoDeCamion, TipoDeCarga, TipoDeEstructuraDeCarga, TipoDeSolicitud } from 'src/environments/constants/enums';
import { QueryParams } from 'src/environments/constants/generalTypes';

@Component({
  selector: 'app-mis-servicios',
  templateUrl: './mis-servicios.page.html',
  styleUrls: ['./mis-servicios.page.scss'],
})
export class MisServiciosPage implements OnInit {
  params: QueryParams = {} as QueryParams
  solicitudOptions: SolicitudOptions
  solicitudes: Solicitud[] = []
  totalItems: number
  constructor(
      private loaderService: LoaderService,
      private solicitudesProvider: SolicitudesProvider,
      private solicitudesProviderMapper: SolicitudesProviderMapper,
      private alertService: AlertService,
      private modal: ModalController
    ) { }

  ngOnInit() {
    this.solicitudOptions = {
      shouldDisplayStatusBar: true,
      modalControllerInstance: this.modal,
      shouldDisplayActionButton: true,
      providerInstance: this.solicitudesProvider,
      modalPageOnClick: undefined,
      loaderInstance: this.loaderService,
      alertInstance: this.alertService,
      propertiesToExclude: []
    }
    this.instanceQueryParams()
    // this.solicitudes = [
    //   {
    //     _id: 'test',
    //     detalleSobrecostos: null,
    //     ofertas: [],
    //     estado: EstadoSolicitud.RECIBIENDO_OFERTAS,
    //     estadoFacturacion: EstadoFacturacion.NO_FACTURADO,
    //     montoSobrecostos: 10000,
    //     nPallets: 2,
    //     mts3: false,
    //     terminadoCliente: false,
    //     montoTotalCliente: 100000,
    //     montoTotalTransportista: 120000,
    //     montoCancelarCliente: 10000,
    //     montoCancelarTransportista: 10000,
    //     montoSobrecostosCliente: 10500,
    //     nombreClienteDeDestino: 'Franco Test',
    //     latDeInicio: -33.0131205,
    //     lonDeInicio: -71.54951989999999,
    //     latDeTermino: -30.6020791,
    //     lonDeTermino: -71.1947606,
    //     direccionDeInicio: 'Avenida Libertad 910, Viña del Mar, Chile',
    //     direccionDeTermino: 'Viana 967 Vina del Mar',
    //     cargaATransportar: 'Madera',
    //     cargaATransportarLista: '3',
    //     valorDeCargaATransportar: 100000,
    //     tipoDeEstructuraDeCarga: TipoDeEstructuraDeCarga.CAJA_CERRADA_CARGA_SECA,
    //     tipoDeCarga: TipoDeCarga.TONELADAS,
    //     cantidadDeCarga: '10',
    //     tipoDeCamion: TipoDeCamion.CAMION_SIMPLE,
    //     dAlto: 3,
    //     dAncho: 2,
    //     dLargo: 10,
    //     necesitaPersonalExtra: 'No',
    //     tipoDeSolicitud: TipoDeSolicitud.SOLICITAR_OFERTAS,
    //     montoNetoInicial: 1000,
    //     disposicionNetaAPagar: 10001000,
    //     duracionDeRuta: '0 Hrs, 6 mins.',
    //     cantidadDeKM: '100 KM',
    //     cliente: {
    //       _id: 'test',
    //       nombre: 'Cliente test'
    //     },
    //     recorrido: [
    //       {
		// 			tipo: "Carga",
		// 			_id: "61aa3998b5b0ad0457abfe06",
		// 			direccion: "Avenida Libertad 910, Viña del Mar, Chile",
		// 			lat: -33.0131205,
		// 			lng: -71.54951989999999,
		// 			nombre: "Juan Fuentealba",
		// 			telefono: "968982954",
		// 			email: "juan.fuentealba@gmail.com"
		// 		},
		// 		{
		// 			tipo: "Descarga",
		// 			_id: "61aa3998b5b0ad0457abfe07",
		// 			direccion: "Antonio Tirado 161, Ovalle, Chile",
		// 			lat: -30.6020791,
		// 			lng: -71.1947606,
		// 			nombre: "Hans Araya",
		// 			telefono: "992437535"
		// 		}],
    //     pasoRecorrido: 0,
    //     camposOpcionales: [{
		// 			titulo: "CPE1",
		// 			valor: "",
		// 		  _id: "61aa3998b5b0ad0457abfe08"
		// 		},
		// 		{
		// 			titulo: "CPE2",
		// 			valor: "",
		// 			_id: "61aa3998b5b0ad0457abfe09"
		// 		},
		// 		{
		// 			titulo: "CPE3",
		// 			valor: "",
		// 			_id: "61aa3998b5b0ad0457abfe0a"
		// 		}],
    //     camposFacturacion: [{
		// 			titulo: "CPE1",
		// 			valor: "",
		// 		  _id: "61aa3998b5b0ad0457abfe08"
		// 		},
		// 		{
		// 			titulo: "CPE2",
		// 			valor: "",
		// 			_id: "61aa3998b5b0ad0457abfe09"
		// 		},
		// 		{
		// 			titulo: "CPE3",
		// 			valor: "",
		// 			_id: "61aa3998b5b0ad0457abfe0a"
		// 		}],
    //     clientePrePago: 1,
    //     comision: '6111ebb8229a2955632175df',
    //     createdAt: '2021-12-03T15:36:56.313Z',
    //     updatedAt: '2021-12-03T15:36:56.313Z',
    //     creadoPor: 'Soc Minera Petreos Quilin Ltda  ',
    //     fechaDeSolicitudDeTransporte: '2021-12-03T16:00:00.902Z',
    //     fechaEstimadaDeTermino: '2021-12-03T22:44:15.062Z',
    //     horaDeLlegada: '2021-12-03T22:44:15.062Z',
    //     idSolicitudDeTransporte: "",
    //     montoSeguroDeCarga: 10000,
    //     numero: "1",
    //     pactando: false,
    //     sugerencia: true,
    //     aceptadoPor: 'test',
    //     acople: 'id',
    //     camion: {patente: 'test'} as Camion,
    //     conductor: {nombreConductor: 'test nombre'} as Conductor,
    //     fechaAgendado: null,
    //     lineaRestada: 10000,
    //     montoNetoPactado: 10000,
    //     transportista: null,
    //     fechaTermino: null,
    //     freemium: false,
    //     valoracion: null,
    //     valoracionCamion: null,
    //     valoracionConductor: null,
    //     fechaCierreSobrecosto: null,
    //     usuarioCierreSobrecosto: null,
    //     autorTerminadoCliente: null,
    //     fechaTerminadoCliente: null,
    //     remitenteTransportista: null
    //   }
    // ]
    this.loadSolicitudes()
  }
  loadSolicitudes($event?){
    if(!$event)
      this.loaderService.presentLoading()
    
    this.solicitudesProvider.getEntries(this.params).then((res) => {
      if(!$event)
        this.loaderService.stopLoader()
      else
        $event.target.complete()

      console.log('res: '+JSON.stringify(res))
      if(res.status == 200) {
        let parsedBody = JSON.parse(res.body)
        let solicitudesDTO = (parsedBody.docs as SolicitudDTO[])
        console.log(JSON.stringify(solicitudesDTO))
        this.totalItems = parsedBody.totalDocs
        this.params.page = parsedBody.page
        this.solicitudes = []
        solicitudesDTO.forEach((destinoDTO) => {
          this.solicitudes.push(this.solicitudesProviderMapper.mapDTOtoObj(destinoDTO))
        })
      }
    }, (err) => {
      console.log('err: '+JSON.stringify(err))
      this.alertService.showAlertOnlyOk('Hubo un error al obtener las solicitudes, pruebe nuevamente o reinicie su sesion', 'close-outline', undefined, 'errorFormAlert')
      if($event)
        $event.target.complete()
    })  
  }
  cambioDePagina(page: number) {
    this.params.page = page;
    this.loadSolicitudes();
  }
  
  instanceQueryParams(){
    this.params.agendadas = true
    this.params.canceladas = true
    this.params.enCurso = true
    this.params.terminadas = true
    this.params.ofertas = true
    this.params.borradores = true
    this.params.limit = 25
    this.params.page = 1
  }
  refrescarPagina($event){
    this.loadSolicitudes($event)
  }
}
