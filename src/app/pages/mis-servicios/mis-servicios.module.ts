import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MisServiciosPageRoutingModule } from './mis-servicios-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';

import { MisServiciosPage } from './mis-servicios.page';
import { SolicitudComponent } from 'src/app/components/solicitud/solicitud.component';
import { ParseIntPipe } from 'src/app/pipes/parse-int.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxPaginationModule,
    MisServiciosPageRoutingModule
  ],
  declarations: [MisServiciosPage, SolicitudComponent, ParseIntPipe]
})
export class MisServiciosPageModule {}
