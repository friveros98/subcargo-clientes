import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MisDatosPage } from './mis-datos.page';

const routes: Routes = [
  {
    path: '',
    component: MisDatosPage
  },
  {
    path: 'editar-perfil',
    loadChildren: () => import('./editar-perfil/editar-perfil.module').then( m => m.EditarPerfilPageModule)
  },
  {
    path: 'editar-datos-bancarios',
    loadChildren: () => import('./editar-datos-bancarios/editar-datos-bancarios.module').then( m => m.EditarDatosBancariosPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MisDatosPageRoutingModule {}
