import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EditarPerfilPage } from './editar-perfil/editar-perfil.page';
import { EditarDatosBancariosPage } from './editar-datos-bancarios/editar-datos-bancarios.page';
// import { DataStorageService } from 'src/app/services/datastorage/datastorage.service';

@Component({
  selector: 'app-mis-datos',
  templateUrl: './mis-datos.page.html',
  styleUrls: ['./mis-datos.page.scss'],
})
export class MisDatosPage implements OnInit {
  user: any;
  tipoUsuario: number
  constructor(
    private modal: ModalController,
    // private dataService: DataStorageService
    ) { }

  ngOnInit() {
    /*this.user = {
      nombre: 'SUBCARGO SPA',
      rut: '77047634-5',
      email: 'dpalacios@subcargo.cl',
      telefono: '222141077',
      direccion: 'Camino Mirasol 1537',
      emailcomercial: 'dpalacios@subcargo.cl',
      nombretitularcuenta: '-',
      
    }*/
    
  }
  // ionViewWillEnter(){
  //   this.cargarDatos(null)
  // }
  // refrescarPagina(event){
  //   this.cargarDatos(event)
  // }
  // cargarDatos(event){
  //   this.dataService.obtenerSesionGuardada().then((session)=>{
  //     if(session){
  //       this.tipoUsuario = this.dataService.tipoUsuario();

  //       let usuario = session.user
  //       let direccion: string;
  //       let emailcomercial: string;
  //       let nombretitularcuenta: string;
  //       let ruttitularcuenta: string;
  //       let banco: string;
  //       let bancoid: string;
  //       let tipodecuenta: string;
  //       let tipodecuentaid: string;
  //       let numerodecuenta: string;
  //       let emailcuenta: string;
  //       let nombre: string
  //       let telefono: string
  //       console.log("TIPO USER: "+this.tipoUsuario)
  //       switch(this.tipoUsuario){
  //         case 1:{
  //           //transportista
  //           direccion = usuario['DireccionComercial']
  //           emailcomercial = usuario['EmailComercial']
  //           nombretitularcuenta = usuario['NombreTitularCuenta']
  //           console.log(""+nombretitularcuenta)
            
  //           ruttitularcuenta = usuario['RutTitularCuenta']
  //           numerodecuenta = usuario['NumeroDeCuenta']
  //           emailcuenta = usuario['CorreoElectronicoComprobante']
  //           tipodecuentaid = usuario['TipoDeCuenta']
  //           bancoid = usuario['Banco']
  //           nombre = usuario['Nombre']
  //           telefono = usuario['Telefono']
  //           switch(usuario['Banco']){
  //             case "1":{
  //               banco = "Banco Santander"
  //               break;
  //             }
  //             case "2":{
  //               banco = "Banco Santander Banefe"
  //               break;
  //             }
  //             case "3":{
  //               banco = "Scotiabank Azul"
  //               break;
  //             }
  //             case "4":{
  //               banco = "Corpbanca"
  //               break;
  //             }
  //             case "5":{
  //               banco = "BCI-TBANC"
  //               break;
  //             }
  //             case "6":{
  //               banco = "Banco Falabella"
  //               break;
  //             }
  //             case "7":{
  //               banco = "Banco Itau"
  //               break;
  //             }
  //             case "8":{
  //               banco = "Banco de Chile / Edwards"
  //               break;
  //             }
  //             case "9":{
  //               banco = "Banco Estado"
  //               break;
  //             }
  //             case "10":{
  //               banco = "Banco Bice"
  //               break;
  //             }
  //             case "11":{
  //               banco = "Banco Security"
  //               break;
  //             }
  //             case "12":{
  //               banco = "Banco Consorcio"
  //               break;
  //             }
  //             case "13":{
  //               banco = "Banco Ripley"
  //               break;
  //             }
  //             case "14":{
  //               banco = "Banco Internacional"
  //               break;
  //             }
  //             case "15":{
  //               banco = "Scotiabank"
  //               break;
  //             }
  //           }
  //           switch(usuario['TipoDeCuenta']){
  //             case "1":{
  //               tipodecuenta = "Cuenta corriente"
  //               break;
  //             }
  //             case "2":{
  //               tipodecuenta = "Cuenta vista"
  //               break;
  //             }
  //             case "3":{
  //               tipodecuenta = "Chequera electrónica"
  //               break;
  //             }
  //             case "4":{
  //               tipodecuenta = "Cuenta de ahorro"
  //               break;
  //             }
  //           }
  //           break;
  //         }
  //         case 2:{
  //           nombre = usuario['NombreConductor']+" "+usuario['ApellidoConductor']
  //           telefono = usuario['TelefonoConductor']
  //           banco = '-'
  //           tipodecuenta = '-'
  //           direccion = '-'
  //           emailcomercial = '-'
  //           nombretitularcuenta = '-'
  //           ruttitularcuenta = '-'
  //           numerodecuenta = '-'
    
  //           break;
  //         }
  //         case 3:{
  //           nombre = usuario['NombreConductor']+" "+usuario['ApellidoConductor']
  //           telefono = usuario['TelefonoConductor']
  //           banco = '-'
  //           tipodecuenta = '-'
  //           direccion = '-'
  //           emailcomercial = '-'
  //           nombretitularcuenta = '-'
  //           ruttitularcuenta = '-'
  //           numerodecuenta = '-'
  //           break;
  //         }
  //       }
  //       console.log("Nombre: "+nombre)
  //       this.user = {
  //         _id: usuario['_id'],
  //         nombre: nombre,
  //         rut: usuario['username'],
  //         email: usuario['email'],
  //         telefono: telefono,
  //         direccion: direccion,
  //         emailcomercial: emailcomercial,
  //         nombretitularcuenta: nombretitularcuenta,
  //         ruttitularcuenta: ruttitularcuenta,
  //         banco: banco,
  //         bancoid: bancoid,
  //         tipodecuenta: tipodecuenta,
  //         tipodecuentaid: tipodecuentaid,
  //         numerodecuenta: numerodecuenta,
  //         emailcuenta: emailcuenta
  //       }
  //       if(event != null)
  //         event.target.complete()
  //     }
  //   })
  // }
  // async modalEditarPerfil(user:any) {
  //   const modal = await this.modal.create({
  //     component: EditarPerfilPage,
  //     componentProps: {
  //       user: user
  //     }
  //   });
  //   modal.onWillDismiss().then(() => {
  //     console.log("probando reload2")
  //     this.cargarDatos(null)
  //   })
  //   return await modal.present();
  // }
  // editarDatos(user: any){
  //   this.modalEditarPerfil(user);
  // }
  // async modalEditarDatosFinancieros(user:any) {
  //   const modal = await this.modal.create({
  //     component: EditarDatosBancariosPage,
  //     componentProps: {
  //       user: user
  //     }
  //   });
  //   modal.onWillDismiss().then(() => {
  //     console.log("probando reload")
  //     this.cargarDatos(null)
  //   })
  //   return await modal.present();
  // }
  // editarDatosFinancieros(){
  //   this.modalEditarDatosFinancieros(this.user);
  // }

  async modalEditarPerfil(user:any) {
    const modal = await this.modal.create({
      component: EditarPerfilPage,
      componentProps: {
        user: user
      }
    });
    modal.onWillDismiss().then(() => {
      
    })
    return await modal.present();
  }
  editarDatos(user: any){
    this.modalEditarPerfil(user);
  }

}
