import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController, AlertController, LoadingController, IonInput } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { RestService } from 'src/app/services/rest/rest.service';
// import { DataStorageService } from 'src/app/services/datastorage/datastorage.service';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.page.html',
  styleUrls: ['./editar-perfil.page.scss'],
})
export class EditarPerfilPage implements OnInit {
  @ViewChild('inputRutUsuario') rutUsuario: IonInput;
  @Input() user: any;
  formDatos: FormGroup;
  errorMessage: any[];

  guionInput: boolean

  constructor(
    private modal: ModalController,
    private formBuilder: FormBuilder,
    private loading: LoadingController,
    private alertController: AlertController,
    // private restService: RestService,
    // private dataService: DataStorageService
    ) { }

  ngOnInit() {
    this.formDatos = this.formBuilder.group({
      nombre: ['', Validators.required],
      rut: ['', Validators.compose([
        Validators.pattern("^\\d{7}(?:\\d{1})?([-])([\\dkK])$"),
        Validators.required
      ])],
      email: ['', Validators.compose([
        Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
        Validators.required
      ])],
      telefono: ['', Validators.required],
      direccion: [''],
      emailcomercial: ['']
    });
    this.errorMessage = [];
    this.errorMessage['nombre'] = 'Debes ingresar una Razón social o el Nombre de una persona natural.'
    this.errorMessage['rut'] = 'El Rut de Empresa o el de una persona natural es requerido.'
    this.errorMessage['email'] = 'El correo electrónico es requerido'
    this.errorMessage['telefono'] = 'Teléfono de contacto es requerido'
    this.errorMessage['rutInvalido'] = 'Rut invalido, use el siguiente formato: (12345678-9).'
    this.errorMessage['emailInvalido'] = 'Correo electrónico invalido'
    this.errorMessage['emailcomercialInvalido'] = 'Correo electrónico comercial invalido'

    this.guionInput = false;
  }
  ionViewWillEnter() {
    this.rutUsuario.readonly = true
    this.formDatos.patchValue({
      nombre: this.user.nombre,
      rut: this.user.rut,
      email: this.user.email,
      telefono: this.user.telefono
    })
    if (this.user.direccion !== "")
      this.formDatos.patchValue({
        direccion: this.user.direccion
      })
    if (this.user.emailcomercial !== "") {
      this.formDatos.patchValue({
        emailcomercial: this.user.emailcomercial
      })
    }
  }
  inputRutChange() {
    if (this.rutUsuario.value != undefined || this.rutUsuario.value != null) {
      let aux: string = this.rutUsuario.value.toString()
      aux = aux.replace("-", "")
      if (aux.length >= 1) {
        let aux2 = aux.match(/([k0-9])/gi)
        let aux3 = aux2.join("")
        aux = aux3
      }
      if (aux.length >= 2) {
        let cuerpoRut: any;
        let dv: any;
        cuerpoRut = aux.slice(0, aux.length - 1)
        dv = aux.slice(aux.length - 1, aux.length)
        aux = cuerpoRut + "-" + dv
      }
      this.rutUsuario.value = aux;
    }
  }
  async guardarDatos() {

    if (this.formDatos.status == "INVALID") {
      let controls = this.formDatos.controls;
      for (let c in controls) {
        if (controls[c].invalid) {
          if (controls[c].errors.pattern) {
            this.popupErrorForm(this.errorMessage[c + 'Invalido'])
            return;
          }

          this.popupErrorForm(this.errorMessage[c])
          return;
        }
      }
    }

    this.presentLoading();
    // this.guardar()
  }
  async popupErrorForm(text: string) {
    const alert = await this.alertController.create({
      cssClass: 'errorFormAlert',
      header: '',
      message: '<ion-icon name="close-outline" class="errorIcon"></ion-icon><br><h1 class="alertBody">' + text + '</h1>',
      buttons: [{
        text: 'Ok'
      }]
    });
    await alert.present();
  }
  async popupAceptado(text: string) {
    const alert = await this.alertController.create({
      cssClass: 'errorFormAlert',
      header: '',
      message: '<ion-icon name="checkmark-outline" class="errorIcon"></ion-icon><br><h1 class="alertBody">' + text + '</h1>',
      buttons: [{
        text: 'Ok'
      }]
    });
    await alert.present();
  }
  async presentLoading(tiempo = 40000) {
    const loading = await this.loading.create({
      translucent: true,
      duration: tiempo
    });
    await loading.present();

    return await loading.onDidDismiss();
  }
  async closeModal() {
    this.modal.dismiss();
  }
  // guardar() {
  //   console.log("DATOS DE EDITARPERFIL:", JSON.stringify(this.formDatos.value));

  //   this.restService.editarDatos(this.formDatos.value).then(
  //     (res) => {
  //       if (res.status == 200) {
  //         this.dataService.actualizarDatosPerfil(this.formDatos.value, res.headers["x-access-token"]).then(() => {
  //           this.popupAceptado("¡Cambio Exitoso! Se han editado los datos correctamente.");
  //           this.modal.dismiss();
  //         })
  //         this.loading.dismiss() //actualiza sesion y refresca token

  //       } else {
  //         this.loading.dismiss()
  //         console.log(res.status)
  //       }
  //     }, (error) => {
  //       this.loading.dismiss()
  //       console.log(JSON.stringify(error))
  //     })
  // }
}
