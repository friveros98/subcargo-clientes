import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditarDatosBancariosPage } from './editar-datos-bancarios.page';

const routes: Routes = [
  {
    path: '',
    component: EditarDatosBancariosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditarDatosBancariosPageRoutingModule {}
