import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarDatosBancariosPageRoutingModule } from './editar-datos-bancarios-routing.module';

import { EditarDatosBancariosPage } from './editar-datos-bancarios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditarDatosBancariosPageRoutingModule
  ],
  declarations: [EditarDatosBancariosPage]
})
export class EditarDatosBancariosPageModule {}
