import { Injectable } from '@angular/core';
import { GetResult, Storage } from '@capacitor/storage';
import { LoggedUsuario } from '../providers/user/user.types';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  user: LoggedUsuario
  constructor() { }

  saveDataInLocalStorage(key, value): Promise<void> {
    return Storage.set({
      key: key,
      value: JSON.stringify(value),
    });
  }
  getValueFromLocalStorage(value: any): Promise<GetResult> {
    return Storage.get({ key: value });
  }
}
