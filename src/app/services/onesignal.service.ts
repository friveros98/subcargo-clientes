import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OneSignal } from '@ionic-native/onesignal/ngx';
@Injectable({
  providedIn: 'root'
})
export class OnesignalService {
  platformType: string;
  constructor(private oneSignal: OneSignal, private router: Router) { }
  setPlatformType(platform: string){
    this.platformType = platform;
  }
  getPlatformType(): string{
    return this.platformType;
  }
  setupOneSignalSystem(id: string){
    this.oneSignal.startInit("25df5de0-4d0d-41f3-acb9-0e5eb557dd2c")
    if(this.platformType == 'ios')
      this.oneSignal.iOSSettings({kOSSettingsKeyAutoPrompt:true,kOSSettingsKeyInAppLaunchURL:false})

    this.oneSignal.setSubscription(true)
    this.oneSignal.setExternalUserId(id)
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    this.oneSignal.handleNotificationOpened().subscribe(() => {
      this.router.navigate(['/tabs/dashboard'],{replaceUrl: true})
    });
    this.oneSignal.endInit();
  }
  disconnectedUser(){
    this.oneSignal.setSubscription(false)
    this.oneSignal.removeExternalUserId()
  }
}
