import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(
    private loadingController: LoadingController
  ) { }
  async presentLoading(tiempo = 10000) {
    const loading = await this.loadingController.create({
      translucent: true,
      duration: tiempo
    });
    await loading.present();

    return await loading.onDidDismiss();
  }
  stopLoader(id?: string){
    this.loadingController.dismiss(id)
  }
}
