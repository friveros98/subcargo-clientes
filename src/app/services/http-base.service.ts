import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpBasicResponse } from 'src/environments/constants/generalTypes';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class HttpBaseService {

  constructor(
    private tokenService: TokenService,
    private HTTP: HTTP
  ) { }

  get(url): Promise<HttpBasicResponse> {
    const options = {
      url: url,
      headers: { "x-access-token": this.tokenService.getCurrentToken() },
    };

    return this.HTTP.get(options.url, {}, options.headers)
    .then((response) => {
      console.log(response)
      let res = {
        status: response.status,
        headers: response.headers,
        body: response.data
      } as HttpBasicResponse
      return response.status > 203 ? Promise.reject(res): Promise.resolve(res)
    });
    
  }
  post(url, body): Promise<HttpBasicResponse> {
    const options = {
      url: url,
      headers: { 'x-access-token': this.tokenService.getCurrentToken() },
      data: body
    };
    return this.HTTP.post(options.url, options.data, options.headers)
    .then((response) => {
      console.log(response)
      let token = response.headers['x-access-token']
      if(token != undefined)
        this.tokenService.setCurrentToken(token)
      let res = {
        status: response.status,
        headers: response.headers,
        body: response.data
      } as HttpBasicResponse
      return response.status > 203 ? Promise.reject(res): Promise.resolve(res)
    });
  }
  delete(url): Promise<HttpBasicResponse> {
    const options = {
      url: url,
      headers: { 'x-access-token': this.tokenService.getCurrentToken() },
    };
    return this.HTTP.delete(options.url, {}, options.headers)
    .then((response) => {
      let token = response.headers['x-access-token']
      if(token != undefined)
        this.tokenService.setCurrentToken(token)
      let res = {
        status: response.status,
        headers: response.headers,
        body: response.data
      } as HttpBasicResponse
      return response.status > 203 ? Promise.reject(res): Promise.resolve(res)
    });
  }
  put(url, body): Promise<HttpBasicResponse> {
    const options = {
      url: url,
      headers: { 'x-access-token': this.tokenService.getCurrentToken() },
      data: body
    };
    return this.HTTP.put(options.url, options.data, options.headers)
    .then((response) => {
      let token = response.headers['x-access-token']
      if(token != undefined)
        this.tokenService.setCurrentToken(token)
      let res = {
        status: response.status,
        headers: response.headers,
        body: response.data
      } as HttpBasicResponse
      return response.status > 203 ? Promise.reject(res): Promise.resolve(res)
    });
  }
}
