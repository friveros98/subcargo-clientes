import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private alertController: AlertController
  ) { }

  async showAlertOkCancel(message: string, buttonOkCallback?, buttonCancelCallback?, cssClass?: string, header?:string, subheader?: string) {
    const alert = await this.alertController.create({
      cssClass: cssClass,
      header: header,
      subHeader: subheader,
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'cancel-btn',
          id: 'cancel-button',
          handler: buttonCancelCallback
        }, {
          text: 'Ok',
          id: 'confirm-button',
          cssClass: 'confirm-btn',
          handler: buttonOkCallback
        }
      ]
    });

    await alert.present();
  }
  async showAlertOnlyOk(message: string, icon?:string, buttonOkCallback?, cssClass?: string, header?:string, subheader?: string) {
    const alert = await this.alertController.create({
      cssClass: cssClass,
      header: header,
      subHeader: subheader,
      message: '<ion-icon name="'+icon+'" class="errorIcon"></ion-icon><br><h1 class="alertBody">'+message+'</h1>',
      buttons: [{
          text: 'Ok',
          id: 'confirm-button',
          cssClass: 'confirm-btn',
          handler: buttonOkCallback
        }
      ]
    });

    await alert.present();
  }
}
