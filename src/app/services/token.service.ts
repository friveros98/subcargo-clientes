import { Injectable } from '@angular/core';
import { GetResult, Storage } from '@capacitor/storage';
import { LocalStorageService } from './localstorage.service';
import { constants } from './../../environments/constants/constants'
@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private currentToken: string = '';
  constructor(
    private localStorage: LocalStorageService
  ) { }
  getCurrentToken(): string {
    return this.currentToken;
  }
  async setCurrentToken(token: string): Promise<void> {
    this.currentToken = token
    await this.localStorage.saveDataInLocalStorage(constants.TOKEN, token)
  }
  async getTokenOnLocalStorage() {
    await this.localStorage.getValueFromLocalStorage(constants.TOKEN)

  }
}
