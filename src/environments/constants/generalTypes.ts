export interface HttpBasicResponse {
    status: any,
    headers: any,
    body: any,
    message?: any
}
export interface GenericProvider {
    deleteEntry(id: string): Promise<HttpBasicResponse>
}
export interface QueryParams{
    agendadas: boolean, 
    canceladas: boolean, 
    terminadas: boolean, 
    enCurso: boolean, 
    borradores: boolean,
    ofertas: boolean,
    limit: number, 
    page: number
}